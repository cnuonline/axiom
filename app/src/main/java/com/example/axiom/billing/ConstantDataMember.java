package com.example.axiom.billing;

/**
 * Created by kiran on 16/11/15.
 */
public class ConstantDataMember {
    public static final String REGULAR_FONT_STYLE="fonts/lato_ttf/lato-regular.ttf";
    public static final String HELVETICA_FONT_STYLE= "fonts/helvmn.ttf";
    public static final String OPEN_SANS_REGULAR_FONT_STYLE= "fonts/open-sans/OpenSans-Regular.ttf";
    public static final String OPEN_SANS_SEMIBOLD_FONT_STYLE= "fonts/open-sans/OpenSans-Semibold.ttf";
    public static final String PRO_ID = "product_id";
    public static final String PRO_NAME = "product_name";

    public static final String PRO_FINAL_PRICE ="product_final_price";
    public static final String PRO_SHORT_DESCRI = "short_description";

/*user information for session*/
    public static final String USER_INFO_SESSION = "user_info_session";

    public static final String USER_INFO_FNAME = "user_info_user_fname";
    public static final String USER_INFO_LNAME = "user_info_user_lname";
    public static final String USER_INFO_USER_ID = "user_info_user_id";
    public static final String USER_INFO_USER_NAME = "user_info_user_name";
    public static final String USER_INFO_USER_PASSWORD = "user_info_user_pass";
    public static final String USER_INFO_USER_LOGIN_STATUS = "user_info_login_status";
    public static final String PRO_INITIAL_QUANTITY = "initial_quantity";
    public static final String USER_SPECIALIZATION = "specialization";
    public static final String USER_COMPNAY = "compnay";
    public static final String USER_MOBILENUMBER = "mobileNumber";
    public static String PRO_IMAGE;

    /*data member for billing and shipping address*/
}
