package com.example.axiom.billing;


import android.os.Parcel;

/**
 * Created by kiran on 16/12/15.
 */
public class UserProfileItem {
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String id;
    private  String Specialization;
    private String mobileNumber;
    private String compnayName;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSpecialization() {
        return Specialization;
    }

    public void setSpecialization(String specialization) {
        Specialization = specialization;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCompnayName() {
        return compnayName;
    }

    public void setCompnayName(String compnayName) {
        this.compnayName = compnayName;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    private String login_status;
    private AddressData billingAddress;
    private AddressData shippingAddress;

    public UserProfileItem(String firstname, String id, String lastname, String login_status, String password, String username,String specialization,String mobile,String compnay) {
        this.firstname = firstname;
        this.id = id;
        this.lastname = lastname;
        this.login_status = login_status;
        this.password = password;
        this.username = username;
        this.Specialization=specialization;
        this.compnayName=compnay;
        this.mobileNumber=mobile;
    }

    protected UserProfileItem(Parcel in) {
        username = in.readString();
        password = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        id = in.readString();
        login_status = in.readString();
    }
    public UserProfileItem(String username) {
        this.username = username;
    }


    public String getFirstname() {
        return firstname;
    }


    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }

    public String getLastname() {
        return lastname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBillingAddress(AddressData addressData){
        this.billingAddress = addressData;
    }

    public AddressData getBillingAddress(){
        return this.billingAddress;
    }

    public void setShippingAddress(AddressData addressData){
        this.shippingAddress = addressData;
    }

    public AddressData getShippingAddress(){
        return this.shippingAddress;
    }



}
