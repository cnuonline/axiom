
package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.axiom.MainActivity_Home;
import com.example.axiom.ProductViewActivity;
import com.example.axiom.WarantyActivity;
import com.kloudportal.axiom.powertheater.R;



public class SalesFragment extends Fragment {
	private View parentView;
	TextView toolbartext;
	private Toolbar toolbar;
	private LinearLayout todaytask;
	private Dialog dialog;
	Button enterwaranty,enterwaranty1, enterwaranty2, enterwaranty3,enterwaranty4, enterwaranty5;
	Context mActivity;
    ImageView cartimage, cartimage1, cartimage2, cartimage3, cartimage4, cartimage5 ;
    TextView cartText, cartText1, cartText2, cartText3, cartText4, cartText5 ;
    TextView stockText, stockText1, stockText2, stockText3, stockText4, stockText5 ;
    TextView countText, countText1, countText2, countText3, countText4, countText5 ;
	@SuppressLint("ResourceAsColor") @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mActivity=getActivity();
		parentView = inflater.inflate(R.layout.sales, container, false);
		toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
		toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
		toolbartext.setText("Warranty");
		Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");

		//for crate home button
		AppCompatActivity activity = (AppCompatActivity) getActivity();
		activity.setSupportActionBar(toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);

		enterwaranty1 = (Button)parentView.findViewById(R.id.waranty_button1);

		enterwaranty1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), WarantyActivity.class));
			}
		});
		enterwaranty = (Button)parentView.findViewById(R.id.waranty_button);
		enterwaranty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), WarantyActivity.class));
			}
		});
		enterwaranty2 = (Button)parentView.findViewById(R.id.waranty_button2);
		enterwaranty2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), WarantyActivity.class));
			}
		});
		enterwaranty3 = (Button)parentView.findViewById(R.id.waranty_button3);
		enterwaranty3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), WarantyActivity.class));
			}
		});
        cartimage = (ImageView) parentView.findViewById(R.id.product1);
        cartimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage1 = (ImageView) parentView.findViewById(R.id.product2);
        cartimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage2 = (ImageView) parentView.findViewById(R.id.product4);
        cartimage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage3 = (ImageView) parentView.findViewById(R.id.product3);
        cartimage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });


		cartText = (TextView) parentView.findViewById(R.id.productname);
		cartText1 = (TextView) parentView.findViewById(R.id.productname1);
		cartText2 = (TextView) parentView.findViewById(R.id.productname2);
		cartText3 = (TextView) parentView.findViewById(R.id.productname3);
		stockText= (TextView) parentView.findViewById(R.id.stockleft);
		stockText1= (TextView) parentView.findViewById(R.id.stockleft1);
		stockText2= (TextView) parentView.findViewById(R.id.stockleft2);
		stockText3= (TextView) parentView.findViewById(R.id.stockleft3);
		countText= (TextView) parentView.findViewById(R.id.count);
		countText1= (TextView) parentView.findViewById(R.id.count1);
		countText2= (TextView) parentView.findViewById(R.id.count2);
		countText3= (TextView) parentView.findViewById(R.id.count3);
        enterwaranty.setTypeface(face);
        enterwaranty1.setTypeface(face);
        enterwaranty2.setTypeface(face);
        enterwaranty3.setTypeface(face);
        cartText.setTypeface(face);
        cartText1.setTypeface(face);
        cartText2.setTypeface(face);
        cartText3.setTypeface(face);
		stockText.setTypeface(face);
		stockText1.setTypeface(face);
		stockText2.setTypeface(face);
		stockText3.setTypeface(face);
		countText.setTypeface(face);
		countText1.setTypeface(face);
		countText2.setTypeface(face);
		countText3.setTypeface(face);
		toolbartext.setTypeface(face);
		toolbartext.setTypeface(face);
		return parentView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	private void initView(){

		}




	public void onBackPressed() {
		Intent setIntent = new Intent(Intent.ACTION_MAIN);
		setIntent.addCategory(Intent.CATEGORY_HOME);
		setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(setIntent);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		((MainActivity_Home)getActivity()).openorclose();
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
