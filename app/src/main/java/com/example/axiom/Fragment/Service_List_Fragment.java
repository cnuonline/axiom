package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.Adapter.MyRecycler_service_list_Adapter;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.Service_Activity;
import com.example.axiom.Service_Model;
import com.example.axiom.URLInfoConstants;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class Service_List_Fragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;

   String customerIdValue;
    Context mActivity;
    TextView toolbartext;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog dialog;
    Button serviceButton;
    List<Service_Model> service_modelList =new ArrayList<>();
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.service_list_layout, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        dialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.setMessage("Please Wait Loading.....");
        //for crate home button
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Service/Repair");
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        serviceButton=(Button)parentView.findViewById(R.id.waranty_button);
        mRecyclerView=(RecyclerView)parentView.findViewById(R.id.recyclerview);
        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        getListService();

        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }

    public  void getListService(){

        dialog.show();

        String url= URLInfoConstants.API_URL+"callback&service=service_list&customer_id="+customerIdValue;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        dialog.dismiss();
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONArray jsonArray=jsonObject.getJSONArray("service");
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                String serialNum=jsonObject1.getString("f_serial_no");
                                String reason=jsonObject1.getString("subject");
                                String description=jsonObject1.getString("f_reason_description");
                                String isStstus=jsonObject1.getString("status_id");
                                String updateDate=jsonObject1.getString("updated_at");
                                Service_Model service_model=new Service_Model();
                                service_model.setSerialNum(serialNum);
                                service_model.setReason(reason);
                                service_model.setDesc(description);
                                service_model.setStatus(isStstus);
                                service_model.setUpdatedDate(updateDate);
                                service_modelList.add(service_model);
                            }

                            mRecyclerView.setHasFixedSize(true);
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new MyRecycler_service_list_Adapter(service_modelList);
                            mRecyclerView.setAdapter(mAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) ;

        queue.add(putRequest);

    }


    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }


}
