package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.axiom.AddToCartActivity;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.ProductViewActivity;
import com.kloudportal.axiom.powertheater.R;


public class PurchaseFragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;
    private LinearLayout todaytask;
    private Dialog dialog;
    Context mActivity;
    TextView toolbartext;
    Button addtocartbutton, addtocartbutton1, addtocartbutton2, addtocartbutton3, addtocartbutton4, addtocartbutton5;
    ImageView cartimage, cartimage1, cartimage2, cartimage3, cartimage4, cartimage5 ;
    TextView cartText, cartText1, cartText2, cartText3, cartText4, cartText5 ;
    TextView stockText, stockText1, stockText2, stockText3, stockText4, stockText5 ;
    TextView countText, countText1, countText2, countText3, countText4, countText5 ;
    TextView rupees, rupees1, rupees2, rupees3, rupees4, rupees5 ;
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.fragment_purchase, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Order Now");
        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");

        addtocartbutton = (Button)parentView.findViewById(R.id.cart_button);
        addtocartbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddToCartActivity.class));
            }
        });
        addtocartbutton1 = (Button)parentView.findViewById(R.id.cart_button1);
        addtocartbutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddToCartActivity.class));
            }
        });
        addtocartbutton2 = (Button)parentView.findViewById(R.id.cart_button2);
        addtocartbutton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddToCartActivity.class));
            }
        });
        addtocartbutton3 = (Button)parentView.findViewById(R.id.cart_button3);
        addtocartbutton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddToCartActivity.class));
            }
        });

        cartimage = (ImageView) parentView.findViewById(R.id.product1);
        cartimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage1 = (ImageView) parentView.findViewById(R.id.product2);
        cartimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage2 = (ImageView) parentView.findViewById(R.id.product4);
        cartimage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });
        cartimage3 = (ImageView) parentView.findViewById(R.id.product3);
        cartimage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductViewActivity.class));
            }
        });


        cartText = (TextView) parentView.findViewById(R.id.productname);
        cartText1 = (TextView) parentView.findViewById(R.id.productname1);
        cartText2 = (TextView) parentView.findViewById(R.id.productname2);
        cartText3 = (TextView) parentView.findViewById(R.id.productname3);
        stockText= (TextView) parentView.findViewById(R.id.stockleft);
        stockText1= (TextView) parentView.findViewById(R.id.stockleft1);
        stockText2= (TextView) parentView.findViewById(R.id.stockleft2);
        stockText3= (TextView) parentView.findViewById(R.id.stockleft3);
        countText= (TextView) parentView.findViewById(R.id.count);
        countText1= (TextView) parentView.findViewById(R.id.count1);
        countText2= (TextView) parentView.findViewById(R.id.count2);
        countText3= (TextView) parentView.findViewById(R.id.count3);
        rupees= (TextView) parentView.findViewById(R.id.rupee);
        rupees1= (TextView) parentView.findViewById(R.id.rupee1);
        rupees2= (TextView) parentView.findViewById(R.id.rupee2);
        rupees3= (TextView) parentView.findViewById(R.id.rupee3);
        addtocartbutton.setTypeface(face);
        addtocartbutton1.setTypeface(face);
        addtocartbutton2.setTypeface(face);
        addtocartbutton3.setTypeface(face);
        cartText.setTypeface(face);
        cartText1.setTypeface(face);
        cartText2.setTypeface(face);
        cartText3.setTypeface(face);
        stockText.setTypeface(face);
        stockText1.setTypeface(face);
        stockText2.setTypeface(face);
        stockText3.setTypeface(face);
        countText.setTypeface(face);
        countText1.setTypeface(face);
        countText2.setTypeface(face);
        countText3.setTypeface(face);
        rupees.setTypeface(face);
        rupees1.setTypeface(face);
        rupees2.setTypeface(face);
        rupees3.setTypeface(face);
        toolbartext.setTypeface(face);
        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }




    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}
