package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.axiom.MainActivity_Home;
import com.kloudportal.axiom.powertheater.R;

public class ContactFragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;
    private LinearLayout todaytask;
    private Dialog dialog;
    Button editProfile;
    Context mActivity;
    TextView toolbartext, mailtext,calltext;
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.fragment_contact, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Find Us");
        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        mailtext=(TextView)parentView.findViewById(R.id.mail);
        calltext=(TextView)parentView.findViewById(R.id.call);


        mailtext.setTypeface(face);
        calltext.setTypeface(face);

//        editProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), EditProfileActivity.class));
//            }
//        });

        ImageView imageview =(ImageView)parentView.findViewById(R.id.twitter);
       imageview.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String url = "https://twitter.com/PowerTheatreIn";
               Intent i = new Intent(Intent.ACTION_VIEW);
               i.setData(Uri.parse(url));
               startActivity(i);

           }
       });
        ImageView imageview1 =(ImageView)parentView.findViewById(R.id.insta);
        imageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.instagram.com/powertheatre/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        ImageView imageview2 =(ImageView)parentView.findViewById(R.id.facebook);
        imageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.facebook.com/PowerTheatreIn/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        ImageView imageview3 =(ImageView)parentView.findViewById(R.id.linkedin);
        imageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.linkedin.com/authwall?trk=gf&trkInfo=AQEKR5C7EmayxAAAAWnI6L3Qk2Vea6XO4gGaWn6832VPUvPfd7ADqWioTA_8wCy3iike7yspZoQbpmHt4nTzFtK_ymUadLBKhpcfoN9OQWBGPiOxMfoXX7x5uQ45Rs65ybFhR28=&originalReferer=https://www.powertheatre.com/&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fin%2Faxiom-power-theatre-50a29a16b%2F";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        ImageView imageview4 =(ImageView)parentView.findViewById(R.id.youtube);
        imageview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.youtube.com/channel/UCC-cuiQoyfWLKvaNPKonaBQ";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });




        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }




    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}

