package com.example.axiom.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.axiom.Adapter.MyRecycler_service_list_Adapter;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.MyRecycler_history_list_Adapter;
import com.example.axiom.OrderModel;
import com.example.axiom.Service_Model;
import com.example.axiom.URLInfoConstants;
import com.kloudportal.axiom.powertheater.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class HistoryFragment extends Fragment {

    private View parentView;
    private Toolbar toolbar;
    RecyclerView mRecyclerView;
    TextView imageTitle;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog dialog;
    String customerIdValue="";
     private  int page_number=1;
     private  int item_count=10;
     //variable for pagination
    private  boolean isLoading=true;
     private  int pastVisibleItems,visibleItemCount,total_itemCount,previous_total=0;
     private  int view_threshold=10;

     List<OrderModel> orderModelList =new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView=inflater.inflate(R.layout.fragment_history, container, false);
        dialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.setMessage("Please Wait Loading.....");
        toolbar=(Toolbar)parentView.findViewById(R.id.toolbar);
        imageTitle=(TextView)parentView.findViewById(R.id.toolbar_title);
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        imageTitle.setText("Order/History");
        mRecyclerView=(RecyclerView)parentView.findViewById(R.id.recyclerview);

        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        getListService();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount=mLayoutManager.getChildCount();
                total_itemCount=mLayoutManager.getItemCount();
                int positionView = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                pastVisibleItems=positionView;

                if(dy>0){

                    if(isLoading){
                        if(total_itemCount>previous_total){

                            isLoading=false;
                            previous_total=total_itemCount;


                        }
                    }

                    if(!isLoading && (total_itemCount-visibleItemCount)<=(pastVisibleItems+view_threshold)){

                        page_number++;
                        pagination();
                        isLoading=true;

                    }



                }


            }
        });
        return parentView;
    }
    public  void getListService(){
        dialog.show();
        String url= URLInfoConstants.API_URL+"callback&store=1&service=customerpaidorder&customer_id="+customerIdValue+"&currentpage="+page_number+"+&pagesize="+item_count;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        dialog.dismiss();
                        Log.d("Response", response);
                        try {

                            JSONArray jsonArray=new JSONArray(response);
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject=jsonArray.getJSONObject(i);
                                String orderIdValue=jsonObject.getString("order_id");
                                String statuseValue=jsonObject.getString("status");
                                String quantityValue=jsonObject.getString("quantity");
                               double value= Double.parseDouble(quantityValue);
                                DecimalFormat format = new DecimalFormat("0.#");
                                OrderModel orderModel=new OrderModel();
                                 orderModel.setOrderId(orderIdValue);
                                 orderModel.setOrderStatus(statuseValue);
                                 orderModel.setOrderQuantity(format.format(value));
                                orderModelList.add(orderModel);
                            }
                            mRecyclerView.setHasFixedSize(true);
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter=new MyRecycler_history_list_Adapter(orderModelList);
                            mRecyclerView.setAdapter(mAdapter);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) ;

        queue.add(putRequest);

    }

     public  void pagination(){
            dialog.show();
        String url= URLInfoConstants.API_URL+"callback&store=1&service=customerpaidorder&customer_id="+customerIdValue+"&currentpage="+page_number+"&pagesize="+item_count;
         RequestQueue queue = Volley.newRequestQueue(getActivity());
         StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                 new Response.Listener<String>()
                 {
                     @Override
                     public void onResponse(String response) {
                         // response
                         dialog.dismiss();
                         Log.d("Response", response);
                         try {

                             JSONArray jsonArray=new JSONArray(response);
                             for(int i=0;i<jsonArray.length();i++){
                                 JSONObject jsonObject=jsonArray.getJSONObject(i);
                                 String orderIdValue=jsonObject.getString("order_id");
                                 String statuseValue=jsonObject.getString("status");
                                 String quantityValue=jsonObject.getString("quantity");
                                 double value= Double.parseDouble(quantityValue);
                                 DecimalFormat format = new DecimalFormat("0.#");
                                 OrderModel orderModel=new OrderModel();
                                 orderModel.setOrderId(orderIdValue);
                                 orderModel.setOrderStatus(statuseValue);
                                 orderModel.setOrderQuantity(format.format(value));
                                 orderModelList.add(orderModel);
                             }
                             /* mAdapter=new MyRecycler_history_list_Adapter();
                             ((MyRecycler_history_list_Adapter) mAdapter).addOrders(orderModelList);
                             mRecyclerView.setAdapter(mAdapter);*/
                             mRecyclerView.setHasFixedSize(true);
                             mLayoutManager = new LinearLayoutManager(getActivity());
                             mRecyclerView.setLayoutManager(mLayoutManager);
                             mAdapter=new MyRecycler_history_list_Adapter(orderModelList);
                             mRecyclerView.setAdapter(mAdapter);

                         } catch (JSONException e) {
                             e.printStackTrace();
                         }


                     }
                 },
                 new Response.ErrorListener()
                 {
                     @Override
                     public void onErrorResponse(VolleyError error) {
                         // error
                         dialog.dismiss();
                         Log.d("Error.Response", error.toString());
                     }
                 }
         ) ;

         queue.add(putRequest);



     }

    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
