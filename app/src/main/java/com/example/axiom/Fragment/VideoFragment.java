package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.example.axiom.Adapter.MyRecyclerActivity_Video_ViewAdapter;
import com.example.axiom.Adapter.MyRecycler_service_list_Adapter;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.YoutubeVideoAdapter;
import com.example.axiom.YoutubeVideoModel;
import com.example.axiom.utube.RecyclerViewOnClickListener;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.kloudportal.axiom.powertheater.R;
import com.payu.magicretry.MainActivity;

import java.util.ArrayList;


public class VideoFragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;
    private LinearLayout todaytask;
    private Dialog dialog;
    Context mActivity;
    static final String AUDIO_PATH =
            "http://yourHost/play.mp3";
    private MediaPlayer mediaPlayer;
    private int playbackPosition=0;
    MediaController mediaController;
    // Declare variables
    ProgressDialog pDialog;
    VideoView videoview;
    TextView toolbartext,headerText;
    // Insert your Video URL
    String VideoURL = "https://www.dropbox.com/s/b8jpu1g9t07mco8/App%20V-1.mp4?dl=1";
    VideoView   videoView,videoView1,videoView2;
    private RecyclerView mRecyclerView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> videoNames =new ArrayList<>();
    ArrayList<String> videoUrls =new ArrayList<>();

    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.video_fragment_layout, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Promotional Video");
        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
      //  mRecyclerView=(RecyclerView)parentView.findViewById(R.id.recyclerview);

        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvmn.ttf");
        toolbartext.setTypeface(face);
        setUpRecyclerView();
        populateRecyclerView();
        //  player("https://www.dropbox.com/s/b8jpu1g9t07mco8/App%20V-1.mp4?dl=1");

     /*   // Create a progressbar
        pDialog = new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        // Set progressbar title
        pDialog.setTitle("Axiom");
        // Set progressbar message
        pDialog.setMessage("Loading ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar

        videoNames.add("CqhpNxI8qYw");
       *//* videoNames.add("Just Place and Play");
        videoNames.add("Just Place and Call");*//*
        videoUrls.add("https://www.dropbox.com/s/38twht8oqrrjli4/ptv1.mp4?dl=1");
        videoUrls.add("https://www.dropbox.com/s/7xgmgbya6manr7b/ptv2.mp4?dl=1");
        videoUrls.add("https://www.dropbox.com/s/5qrynhkjw8sdvir/ptv.mp4?dl=1");
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyRecyclerActivity_Video_ViewAdapter(videoNames);
        mRecyclerView.setAdapter(mAdapter);
        ((MyRecyclerActivity_Video_ViewAdapter)mAdapter).setOnItemClickListener(new MyRecyclerActivity_Video_ViewAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                      openALertDialog(videoUrls.get(position));
              //  startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/embed/bmDxSdlR7Ck")));
                Log.i("Video", "Video Playing....");
            }


        });*/

      // playerVideo("https://www.dropbox.com/s/38twht8oqrrjli4/ptv1.mp4?dl=1");











        return parentView;
    }



    public void openALertDialog(String videoURL ) {

        pDialog.show();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_list, null);
        dialogBuilder.setView(dialogView);

        ImageView closeButton=(ImageView)dialogView.findViewById(R.id.close);

         videoView=(VideoView)dialogView.findViewById(R.id.video);


          playerVideo(videoURL);


        final AlertDialog alertDialog = dialogBuilder.create();
        final Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    public void playerVideo(String url){


        if (mediaController == null) {
            // create an object of media controller class
            mediaController = new MediaController(getActivity());
            mediaController.setAnchorView(videoView);
        }
        // set the media controller for video view
        videoView.setMediaController(mediaController);
        // set the uri for the video view
        videoView.setVideoURI(Uri.parse(url));
        // start a video
        videoView.seekTo(1);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoView.start();
            }
        });
        // implement on completion listener on video view
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                pDialog.dismiss();
                videoView.pause();
                Toast.makeText(getActivity(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                pDialog.dismiss();
                Toast.makeText(getActivity(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });
    }
    /**
     * setup the recyclerview here
     */
    private void setUpRecyclerView() {
        recyclerView = (RecyclerView)parentView.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    /**
     * populate the recyclerview and implement the click event here
     */
    private void populateRecyclerView() {
        final ArrayList<YoutubeVideoModel> youtubeVideoModelArrayList = generateDummyVideoList();
        YoutubeVideoAdapter adapter = new YoutubeVideoAdapter(getActivity(), youtubeVideoModelArrayList);
        recyclerView.setAdapter(adapter);

        //set click event
        recyclerView.addOnItemTouchListener(new RecyclerViewOnClickListener(getActivity(), new RecyclerViewOnClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //start youtube player activity by passing selected video id via intent
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(),
                        "AIzaSyA5kyaLgS7MKxS19uHf2CUsIOmDkv92DGU", youtubeVideoModelArrayList.get(position).getVideoId(), 100, false, true);
                startActivity(intent);

            }
        }));
    }


    /**
     * method to generate dummy array list of videos
     *
     * @return
     */
    private ArrayList<YoutubeVideoModel> generateDummyVideoList() {
        ArrayList<YoutubeVideoModel> youtubeVideoModelArrayList = new ArrayList<>();

        //get the video id array, title array and duration array from strings.xml
        String[] videoIDArray = getResources().getStringArray(R.array.video_id_array);
        String[] videoTitleArray = getResources().getStringArray(R.array.video_title_array);
        String[] videoDurationArray = getResources().getStringArray(R.array.video_duration_array);

        //loop through all items and add them to arraylist
        for (int i = 0; i < videoIDArray.length; i++) {

            YoutubeVideoModel youtubeVideoModel = new YoutubeVideoModel();
            youtubeVideoModel.setVideoId(videoIDArray[i]);
            youtubeVideoModel.setTitle(videoTitleArray[i]);
            youtubeVideoModel.setDuration(videoDurationArray[i]);

            youtubeVideoModelArrayList.add(youtubeVideoModel);

        }

        return youtubeVideoModelArrayList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }




    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}