package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.MainActivity_Home;
import com.example.axiom.Service_Activity;
import com.example.axiom.URLInfoConstants;
import com.kloudportal.axiom.powertheater.R;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class ServiceFragment extends Fragment {
    private View parentView;
    Context mActivity;
    EditText cust_mobile,cust_Sno,cust_desc;
    Button cusyt_Button;
    String customerIdValue;
    Spinner spinner;
    ProgressDialog dialog;
    private Toolbar toolbar;
    TextView toolbartext;
    ImageView list;
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.service, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        //for crate home button
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Service/Repair");
        list=(ImageView)toolbar.findViewById(R.id.imagelist);
        list.setImageResource(R.drawable.servicelist);
        dialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.setMessage("Please wait Loading.....");
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        // Spinner element
        spinner = (Spinner)parentView.findViewById(R.id.spinner2);

        // Spinner click listener


        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Select Reason");
        categories.add("Not Working");
        categories.add("Power bank is not working");
        categories.add("Amplification issue");
        categories.add("Others");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item_text,categories);

        // attaching data adapter to spinner
        spinner.setAdapter(adapter);
         cust_Sno=(EditText)parentView.findViewById(R.id.Serialno);
         cust_mobile=(EditText)parentView.findViewById(R.id.cus_mobile);
         cust_desc=(EditText)parentView.findViewById(R.id.phone);
         cusyt_Button=(Button) parentView.findViewById(R.id.register_button);


        cust_Sno.setTypeface(face);
        cust_mobile.setTypeface(face);
        cust_desc.setTypeface(face);


        cusyt_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(cust_desc.getText().toString().trim())&&!TextUtils.isEmpty(cust_Sno.getText().toString().trim())&&!TextUtils.isEmpty(cust_mobile.getText().toString().trim())&& !spinner.getSelectedItem().toString().equalsIgnoreCase("Select Reason")){
                    postSubmitService();
                }else{
                    Toast.makeText(getActivity(),"Please Enter all the Fields",Toast.LENGTH_LONG).show();
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Service_Activity.class));
            }
        });


        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }

    public  void postSubmitService(){

        dialog.show();

        String url= URLInfoConstants.API_URL+"callback&service=service_ticket&store=1";
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        dialog.dismiss();
                        Log.d("Response", response);
                        Toast.makeText(getActivity(),"Service Ticket is Sucessfully Submitted",Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                //&customer_id=12&reason=display&description=testing&telephone=9948484627&serial_no=12345"
                params.put("customer_id",customerIdValue);
                params.put("reason",spinner.getSelectedItem().toString());
                params.put("description",cust_desc.getText().toString().trim());
                params.put("telephone",cust_mobile.getText().toString().trim());
                params.put("serial_no",cust_Sno.getText().toString().trim());


                return params;
            }

        };

        queue.add(putRequest);

    }


    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }


}
