package com.example.axiom.Fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.Adapter.MyRecyclerActivityViewAdapter;
import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.AddCart.ShoppingCartItem;
import com.example.axiom.AddCart.ShoppingItem;
import com.example.axiom.AddCart.ShoppingItemManager;
import com.example.axiom.AddCart.SimpleShoppingItem;
import com.example.axiom.AddToCartActivity;
import com.example.axiom.AdditionalInfoItem;
import com.example.axiom.BannerAdapter;
import com.example.axiom.BannerItem;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.ProductViewActivity;
import com.example.axiom.URLInfoConstants;
import com.example.axiom.Validation;
import com.kloudportal.axiom.powertheater.R;

import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HomeFragment extends android.support.v4.app.Fragment implements AbsListView.OnScrollListener {

    private ViewPager mPager;
    private CirclePageIndicator indicator;
    private Handler handler;
    private int slideImgCounter;
    private final String TAG = "HomeFragment";
    private boolean reverseFlag;
    private ArrayList<BannerItem> bannerListData;

    private ArrayList<ShoppingItem> newProductListDetails;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private BannerAdapter adapter;
    private View rootView;
    android.support.v7.widget.Toolbar toolbar;
    public static ProgressDialog pDialog;
    private static final int numberOfItem = 1;
    public static ArrayList<AdditionalInfoItem> additionalInfoList;
    public static String cartImage;
    private String PRODUCT_ID;
    private String PRODUCT_NAME = "";
    private String type;
    private String has_custom_option,description;
    private ArrayList<ShoppingCartItem> cartItemList;
    private TextView txtV_price;
    private TextView txtV_description;
    private String short_description;
    private ShoppingItem currentItem = null;
    private ShoppingItem reorderItem;
    private ArrayList<String> imgListStrArry;
    ImageView cartImages;
    Context context;
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			/*
				Hide menu's back button
			 */

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home, null);
            Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvmn.ttf");
            toolbar = (android.support.v7.widget.Toolbar) rootView.findViewById(R.id.toolbar);
            AppCompatActivity activity = (AppCompatActivity) context;
            activity.setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
             cartImages=(ImageView)toolbar.findViewById(R.id.cart);
            initialized();
            getViewControlls(rootView);


            setViewPager();


            moveBannerImages();
            getHomeProductDetails();

            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {


                @Override
                public void onPageSelected(int arg0) {
                    slideImgCounter = arg0;
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    //Log.d(TAG, "onPageScrolled "+arg0);
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                    //Log.d(TAG, "onPageScrollStateChanged "+arg0);

                }
            });
        }
         cartImages.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 startActivity(new Intent(context, AddToCartActivity.class));
             }
         });

        return rootView;
    }


    private void moveBannerImages() {
        Log.d("HomeFragment", "Enter in Thread Handler");
        Runnable runnable = new Runnable() {

            public void run() {
                Log.d("HomeFragment", "Running in Thread Handler");
                if (slideImgCounter == bannerListData.size() - 1)
                    reverseFlag = true;
                if (slideImgCounter == 0)
                    reverseFlag = false;


                mPager.setCurrentItem(slideImgCounter, true);
                //indicator.setViewPager(mPager);
                if (reverseFlag)
                    slideImgCounter--;
                else
                    slideImgCounter++;


                handler.postDelayed(this, 5000);
            }
        };
        runnable.run();
    }


    private void initialized() {
        handler = new Handler();
        bannerListData = new ArrayList<BannerItem>();
        newProductListDetails = new ArrayList<ShoppingItem>();

    }

    private void setViewPager() {
        bannerListData = new ArrayList<>();
        BannerItem bannerItem = new BannerItem();
        bannerItem.setBannrImage(R.drawable.banner2);
        BannerItem bannerItem1 = new BannerItem();
        bannerItem1.setBannrImage(R.drawable.banner1);
        BannerItem bannerItem2 = new BannerItem();
        bannerItem2.setBannrImage(R.drawable.banner2);
        BannerItem bannerItem3 = new BannerItem();
        bannerItem3.setBannrImage(R.drawable.banner1);
        bannerListData.add(bannerItem);
        bannerListData.add(bannerItem1);
        bannerListData.add(bannerItem2);
        bannerListData.add(bannerItem3);
        adapter = new BannerAdapter(getActivity(), bannerListData);
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(0, true);

        CirclePageIndicator mIndicator = indicator;
        indicator.setViewPager(mPager);
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            final float density = getResources().getDisplayMetrics().density;

            indicator.setRadius(3.2f * density);
        }
        indicator.setPageColor(0xFFCBCBCB);
        indicator.setFillColor(Color.BLACK);
        indicator.setStrokeColor(0xFFCBCBCB);
        indicator.setSnap(true);
        indicator.setStrokeWidth(1);
    }





    private void getViewControlls(View rootView) {
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        mRecyclerView=(RecyclerView)rootView.findViewById(R.id.recyclerview);
        pDialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        pDialog.setMessage("Loading ...");

        }

        public void getHomeProductDetails(){
          pDialog.show();
            String url= URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=getProductList";

            StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                       Log.d("HomeResponse",response.toString());
                    if(pDialog.isShowing())
                        pDialog.hide();
                    try {
                        JSONObject strJSNobj = new JSONObject(response);
                       JSONArray  imageJSNArray = strJSNobj.getJSONArray("products_list");
                        newProductListDetails = parseProducts(imageJSNArray);

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new MyRecyclerActivityViewAdapter(newProductListDetails);
                    mRecyclerView.setAdapter(mAdapter);
                    ((MyRecyclerActivityViewAdapter) mAdapter).setOnItemClickListener(new MyRecyclerActivityViewAdapter.MyClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Intent intent=new Intent(context, ProductViewActivity.class);
                            intent.putExtra("productid",newProductListDetails.get(position).getId());
                            intent.putExtra("productname",newProductListDetails.get(position).getName());
                            context.startActivity(intent);

                        }

                        @Override
                        public void onItemClickBuyNow(int position, View v) {
                            if(newProductListDetails.get(position).getId().equalsIgnoreCase("1")){
                                getProductViewDetails(newProductListDetails.get(position).getId());

                            }else{
                                startActivity(new Intent(getActivity(),MainActivity_Home.class));
                                getActivity().finish();
                            }
                        }

                        @Override
                        public void onItemImageclick(int position, View v) {
                            Intent intent=new Intent(context, ProductViewActivity.class);
                            intent.putExtra("productid",newProductListDetails.get(position).getId());
                            intent.putExtra("productname",newProductListDetails.get(position).getName());
                            context.startActivity(intent);
                        }
                    });



                    Log.d("Volley Size ", newProductListDetails.size() + "");

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("errorHome",error.toString());
                    if(pDialog.isShowing())
                        pDialog.hide();
                }
            });
            if(context !=null){
                Volley.newRequestQueue(context).add(stringRequest);
            }




            }


    private ArrayList<ShoppingItem> parseProducts(JSONArray jsonArray){
        ArrayList<ShoppingItem> items = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); ++i) {
            try {
                ShoppingItem item = ShoppingItem.create(jsonArray.getJSONObject(i));
                if(item != null){
                    items.add(item);
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        return items;
    }







    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        // MainActivity_Home.INSTANCE.showDrawerOption(false);
        super.onStop();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    public void getProductViewDetails(final String idValue){

        final String url = URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=productdetaildescription&productid=" + idValue;

        StringRequest stringRequest =new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("reponseproductdetails",response);



                try {
                    JSONObject strJSNobj = new JSONObject(response);
                    String name = strJSNobj.getString("name");
                    String quantity = strJSNobj.getString("quantity");
                    String price = strJSNobj.getString("price");
                    String sprice = strJSNobj.getString("sprice");
                    String sku = strJSNobj.getString("sku");
                    description = strJSNobj.getString("description");
                    short_description = strJSNobj.getString("shortdes");
                    int maxAllQuantity=10;
                    if(strJSNobj.has("max_allowed_quantity")) {
                        maxAllQuantity = strJSNobj.getInt("max_allowed_quantity");
                    }
                    cartImage = strJSNobj.getString("img");
                    String url = strJSNobj.getString("url");
                    Log.d("onlyurl ", url);
                    type = strJSNobj.getString("type");

                    has_custom_option = strJSNobj.getString("has_custom_option");

                    if (!Validation.isNull(idValue, name, sku, cartImage, price, sprice, quantity)) {
                        if(!type.equals("downloadable"))
                        {
                            currentItem = new SimpleShoppingItem(idValue, name, sku, cartImage, price, sprice, "1", quantity, type, url,maxAllQuantity);// edited by prashant
                            ShoppingItemManager.getInstance().addShoppingItem(currentItem);

                        }
                        addItemToCart();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("erroreproductdetails",error.toString());
            }
        });
        if(getActivity()!=null){
            Volley.newRequestQueue(context).add(stringRequest);
        }


    }
    private void addItemToCart() {
        if (currentItem != null || reorderItem != null) {
            ShoppingCartItem item;
            if (currentItem != null) {
                item = new ShoppingCartItem(currentItem, numberOfItem);
            } else {
                item = new ShoppingCartItem(reorderItem, numberOfItem);
            }
            //test

            Collection<ShoppingCartItem> sp = ShoppingCart.getInstance().getCartItems();
            int numofcartitems = sp.size();
            if (numofcartitems != 0) {
                int FLAG = 0;
                for (int i = 0; i < sp.size(); i++) {
                    Iterator<ShoppingCartItem> it = sp.iterator();
                    while (it.hasNext()) {
                        ShoppingCartItem sci = it.next();
                        if (currentItem != null) {
                            if (sci.getShoppingItem().getId().equals(currentItem.getId())) {
                                Activity activity = getActivity();
                                if(activity!=null) {
                                    final Toast toast = Toast.makeText(activity, "Item already added", Toast.LENGTH_SHORT);
                                    toast.show();

                                }
                                FLAG = 0;
                                break;
                            } else {
                                FLAG = 1;
                            }
                        } else {
                            if (sci.getShoppingItem().getId().equals(reorderItem.getId())) {
                                FLAG = 0;
                                break;
                            } else {
                                FLAG = 1;
                            }
                        }
                    }

                }

                if (FLAG == 1) {
                    ShoppingCart.getInstance().addItem(item);
                }
            } else {
                ShoppingCart.getInstance().addItem(item);
            }

            //end test
            cartItemList = new ArrayList<>(ShoppingCart.getInstance().getCartItems());

            if (cartItemList.size() > 0) {
               /* MainActivity_Home.txtV_item_counter.setText(cartItemList.size() + "");
                MainActivity_Home.txtV_item_counter.setText(cartItemList.size() + "");
                MainActivity_Home.txtV_item_counter.setVisibility(View.VISIBLE);*/
            } else {
                /*MainActivity_Home.txtV_item_counter.setVisibility(View.INVISIBLE);*/
            }
            //  new MyCartFragment().setCounterItemAddedCart(cartItemList);
            // callFragment(new MyCartFragment());
            startActivity(new Intent(context, AddToCartActivity.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onAttach(Context context) {
        this.context=context;
        super.onAttach(context);
    }


}