package com.example.axiom.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.Adapter.MyRecycler_warranty_list_Adapter;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.URLInfoConstants;
import com.example.axiom.WarantyActivity;
import com.example.axiom.Warranty_Model;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class Warranty_List_Fragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;

   String customerIdValue;
    Context mActivity;
    TextView toolbartext;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog dialog;
    Button warrantyButton;
    List<Warranty_Model> warranty_modelList=new ArrayList<>();
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.waranty_list_layout, container, false);
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        dialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.setMessage("Please Wait Loading.....");
        //for crate home button
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Warranty");
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);

         mRecyclerView=(RecyclerView)parentView.findViewById(R.id.recyclerview);
         warrantyButton=(Button)parentView.findViewById(R.id.waranty_button);
         warrantyButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                if(!runtime_permissions())
                    nextactivity();
             }
         });

        getListWaranty();

        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }

    public  void getListWaranty(){

        dialog.show();

        String url= URLInfoConstants.API_URL+"callback&service=warranty_list&customer_id="+customerIdValue;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        dialog.dismiss();
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            Warranty_Model warranty_model=new Warranty_Model();
                            JSONArray jsonArray=jsonObject.getJSONArray("warranty");
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                String serialNum=jsonObject1.getString("f_serial_no");
                                String saleDate=jsonObject1.getString("created_at");
                                warranty_model.setSerialNum(serialNum);
                                warranty_model.setSaleDate(saleDate);
                                warranty_modelList.add(warranty_model);
                            }

                            mRecyclerView.setHasFixedSize(true);
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new MyRecycler_warranty_list_Adapter(warranty_modelList);
                            mRecyclerView.setAdapter(mAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) ;

        queue.add(putRequest);

    }


    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
    private boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            //Alert boz to show user what permissions has to be granted
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("App Permissions");
//            alertDialogBuilder.setMessage(R.string.ALLinone);
            alertDialogBuilder.setCancelable(false);
            //If user allow ask permission
            alertDialogBuilder.setPositiveButton("Allow",
                    new DialogInterface.OnClickListener() {

                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            requestPermissions(new String[]{
                                    Manifest.permission.CAMERA
                            }, 1);
                        }
                    });

            alertDialogBuilder.setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //If user deny then close app
                   getActivity(). finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


            return true;
        }
        return false;
    }
    public  void nextactivity(){
        startActivity(new Intent(getActivity(), WarantyActivity.class));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                nextactivity();


            }
            else
            {
                runtime_permissions();

            }
        }
    }

}
