package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.EditProfileActivity;
import com.example.axiom.MainActivity_Home;
import com.example.axiom.PersistanceForProfile;
import com.example.axiom.ProfileModel;
import com.example.axiom.URLInfoConstants;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class ProfileFragment extends Fragment {
    private View parentView;
    private Toolbar toolbar;
    private LinearLayout todaytask;
    private Dialog dialog;
    Button editProfile;
    ProgressDialog progressDialog;
    Context mActivity;
    String customerIdValue="";
    ArrayList<ProfileModel> profileModelArrayList=new ArrayList<>();
    TextView toolbartext, gstntext,gstnvalue,storetext,storevalue,nametext,namevalue,phonetext,phonevalue,addresstxt,addressval,emailtext,emailvalue,profileheader,secText,secValue;
    @SuppressLint("ResourceAsColor") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity=getActivity();
        parentView = inflater.inflate(R.layout.fragment_profile, container, false);
        progressDialog=new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
                           progressDialog.setMessage("Please wait Loading....");
        toolbar = (Toolbar) parentView.findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Profile");
        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        gstntext=(TextView)parentView.findViewById(R.id.gst);
        gstnvalue=(TextView)parentView.findViewById(R.id.gstn);
        storetext=(TextView)parentView.findViewById(R.id.storetext);
        storevalue=(TextView)parentView.findViewById(R.id.store);
        nametext=(TextView)parentView.findViewById(R.id.nametext);
        namevalue=(TextView)parentView.findViewById(R.id.name);
        phonetext=(TextView)parentView.findViewById(R.id.phonetext);
        phonevalue=(TextView)parentView.findViewById(R.id.phone_num);
        emailtext=(TextView)parentView.findViewById(R.id.emailtxt);
        emailvalue=(TextView)parentView.findViewById(R.id.email_address);
        profileheader=(TextView)parentView.findViewById(R.id.profileheader);
        addresstxt=(TextView)parentView.findViewById(R.id.addresstext);
        addressval=(TextView)parentView.findViewById(R.id.addressvalue);
        secText=(TextView)parentView.findViewById(R.id.phonetextsec);
        secValue=(TextView)parentView.findViewById(R.id.phone_num_sec);

        gstntext.setTypeface(face);
        gstnvalue.setTypeface(face);
        storetext.setTypeface(face);
        storevalue.setTypeface(face);
        nametext.setTypeface(face);
        namevalue.setTypeface(face);
        phonetext.setTypeface(face);
        phonevalue.setTypeface(face);
        addresstxt.setTypeface(face);
        addressval.setTypeface(face);
        emailtext.setTypeface(face);
        emailvalue.setTypeface(face);
        toolbartext.setTypeface(face);
        profileheader.setTypeface(face);
        secText.setTypeface(face);
        secValue.setTypeface(face);
        editProfile = (Button)parentView.findViewById(R.id.edit_button);
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        getProfileData(customerIdValue);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(profileModelArrayList.size()>0){
                    for(int i=0;i<profileModelArrayList.size();i++){
                        PersistanceForProfile.getInstance().setProfileModel(profileModelArrayList.get(i));
                        startActivity(new Intent(getActivity(), EditProfileActivity.class));
                    }
                }
//                PersistanceForProfile.getInstance().setProfileModel(profileModelArrayList.get(0));
//                startActivity(new Intent(getActivity(), EditProfileActivity.class));
            }
        });




        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(){

    }

      public void getProfileData(String idValue){
             progressDialog.show();
            String url= URLInfoConstants.API_URL+"callback&service=user_profile&store=1&customer_id="+idValue;
          StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
              @Override
              public void onResponse(String response) {
                  Log.d("profileResponse",response);
                  if(progressDialog.isShowing())
                      progressDialog.dismiss();
                  ProfileModel profileModel=new ProfileModel();
                  try {
                      JSONObject jsonObject=new JSONObject(response);
                      String pName=jsonObject.getString("name");
                      String pEmail=jsonObject.getString("email");
                      String pFMobile=jsonObject.getString("telephone");
                      String pSMobile=jsonObject.getString("second_number");
                      String pstoreName=jsonObject.getString("store_name");
                      String pGNo=jsonObject.getString("gstin_no");
                      String paddress=jsonObject.getString("store_address");
                      gstnvalue.setText(pGNo);
                      storevalue.setText(pstoreName);
                      namevalue.setText(pName);
                      emailvalue.setText(pEmail);
                      phonevalue.setText(pFMobile);
                      addressval.setText(paddress);
                      secValue.setText(pSMobile);
                      profileModel.setStoreGst(pGNo);
                      profileModel.setStoreName(pstoreName);
                      profileModel.setPersonName(pName);
                      profileModel.setsEmail(pEmail);
                      profileModel.setPrimaryNumber(pFMobile);
                      profileModel.setSecoundaryNumber(pSMobile);
                      profileModel.setAddress(paddress);
                      profileModelArrayList.add(profileModel);




                  } catch (JSONException e) {
                      e.printStackTrace();
                  }
              }
          }, new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {
                  if(progressDialog.isShowing())
                      progressDialog.dismiss();
                  Log.d("profileError",error.toString());

              }
          });

          Volley.newRequestQueue(getActivity()).add(stringRequest);






      }



    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ((MainActivity_Home)getActivity()).openorclose();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
