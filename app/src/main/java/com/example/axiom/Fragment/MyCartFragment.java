package com.example.axiom.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.axiom.Adapter.ShoppingCartAdapter;
import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.AddCart.ShoppingCartItem;
import com.example.axiom.BillingAddress;
import com.example.axiom.MainActivity_Home;
import com.kloudportal.axiom.powertheater.R;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by kiran on 6/11/15.
 */
public class MyCartFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    private static String TAG = "";
    private TextView tct_my_cart;
    private RelativeLayout rl_cart_item_available;
    private RelativeLayout rl_no_item_in_my_cart;
    private ListView listV_my_cart;
    private ShoppingCartAdapter adapter;
    private TextView tct_continue_shoping;
    private int counter = 0;
    private ArrayList<ShoppingCartItem> cartItemList;
    private TextView txtV_total_price;
    private Typeface lat0_Font_ttf, tf;
    private TextView txtV_total;
    private SharedPreferences mySharedPreference;
    private TextView txtV_check_out;

    @SuppressLint("ValidFragment")
    public MyCartFragment() {
        //Empty
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootvView = inflater.inflate(R.layout.add_to_cart, null);

        initialized();
        getViewControll(rootvView);
        setFontStyle();
        cartItemList = new ArrayList<>(ShoppingCart.getInstance().getCartItems());
        setTotalPrice();
        setCounterItemAddedCart();
        setDataToAdapter();

        return rootvView;
    }

    private void setCounterItemAddedCart() {
        if (cartItemList.size() > 0) {
           // MainActivity_Home.txtV_item_counter.setText(cartItemList.size() + "");
          //  MainActivity_Home.txtV_item_counter.setVisibility(View.VISIBLE);
            rl_cart_item_available.setVisibility(View.VISIBLE);
            rl_no_item_in_my_cart.setVisibility(View.GONE);
        } else {
          //  MainActivity_Home.txtV_item_counter.setVisibility(View.INVISIBLE);
            rl_no_item_in_my_cart.setVisibility(View.VISIBLE);
            rl_cart_item_available.setVisibility(View.GONE);
        }
    }

    private void setFontStyle() {
        txtV_total_price.setTypeface(lat0_Font_ttf);
        tct_continue_shoping.setTypeface(lat0_Font_ttf);
        txtV_check_out.setTypeface(lat0_Font_ttf);
        txtV_total.setTypeface(tf);
    }

    private void removeItemFromCart(ShoppingCartItem cartItem, int position) {
        ShoppingCart.getInstance().deleteItem(cartItem);
        cartItemList.remove(position);
        adapter.notifyDataSetChanged();

        setTotalPrice();
    }

    private void setTotalPrice() {
        double totalPrice = ShoppingCart.getInstance().getSubTotal();
        txtV_total_price.setText("\u20b9"+(totalPrice));
    }

    public void addItemValueToCart(ShoppingCartItem cartItem, int position) {
        int currentCount = cartItem.getCount();
        //String type=cartItem.getShoppingItem().getType();
        int available=  cartItem.getShoppingItem().getStockQuantity();
        int maxQuantity=cartItem.getShoppingItem().getmaxAllwoedQuantity();

        if (currentCount < cartItem.getShoppingItem().getStockQuantity()&&currentCount<maxQuantity) {
            ++currentCount;
            cartItem = new ShoppingCartItem(cartItem.getShoppingItem(), currentCount);
            ShoppingCart.getInstance().addItem(cartItem);
            cartItemList.remove(position);
            cartItemList.add(position, cartItem);
           adapter.notifyDataSetChanged();
            setTotalPrice();
        } else {
            Activity activity = getActivity();
            if(isAdded()&&activity!=null)
            {
                int quantity=0;
                if(available<maxQuantity)
                    quantity=available;
                else
                    quantity=maxQuantity;

                String maxQauntity=getResources().getString(R.string.max_quantity);
                maxQauntity=String.format(maxQauntity,String.valueOf(quantity));
                Toast.makeText(getActivity(),maxQauntity,Toast.LENGTH_SHORT).show();
            }

        }
    }

    public  void lessItemCountToCart(ShoppingCartItem cartItem, int position) {
        int currentCount = cartItem.getCount();

        if (currentCount > 1) {
            --currentCount;
            cartItem = new ShoppingCartItem(cartItem.getShoppingItem(), currentCount);
            ShoppingCart.getInstance().addItem(cartItem);
            cartItemList.remove(position);
            cartItemList.add(position, cartItem);
            adapter.notifyDataSetChanged();
            setTotalPrice();
        } else {
            Activity activity = getActivity();
            if(isAdded()&&activity!=null)
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.least_quantity_selected), Toast.LENGTH_SHORT).show();
        }
    }

    private View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    private void setDataToAdapter() {
        adapter = new ShoppingCartAdapter(getActivity(), cartItemList, MyCartFragment.this);
        listV_my_cart.setAdapter(adapter);
    }

    private void getViewControll(View rootvView) {


        rl_cart_item_available = (RelativeLayout) rootvView.findViewById(R.id.rl_cart_item_available);
        rl_no_item_in_my_cart = (RelativeLayout) rootvView.findViewById(R.id.rl_no_item_in_my_cart);

        listV_my_cart = (ListView) rootvView.findViewById(R.id.listV_my_cart);

        tct_continue_shoping = (TextView) rootvView.findViewById(R.id.tct_continue_shoping);
        tct_continue_shoping.setOnClickListener(this);



        txtV_check_out = (TextView) rootvView.findViewById(R.id.txtV_check_out);
        txtV_check_out.setOnClickListener(this);



        txtV_total_price = (TextView) rootvView.findViewById(R.id.txtV_total_price);
        txtV_total = (TextView) rootvView.findViewById(R.id.txtV_total);

        txtV_total.setTypeface(tf);

        TextView btn_shop_to_cotinue = (TextView) rootvView.findViewById(R.id.btn_shop_to_cotinue);
        btn_shop_to_cotinue.setOnClickListener(this);
        // set ontouch listener
        //Config.getInstance().getOnTouchFilledBtnForDentist().filledButtonListenerOnTextView(btn_shop_to_cotinue);


    }

    private void initialized() {
        TAG = getClass().getSimpleName();
        lat0_Font_ttf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Regular.ttf");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tct_continue_shoping:
            case R.id.btn_shop_to_cotinue:
             startActivity(new Intent(getActivity(), MainActivity_Home.class));
                break;
            case R.id.txtV_check_out:
               /* if (mySharedPreference.getString(ConstantDataMember.USER_INFO_USER_LOGIN_STATUS, "0").equals("1")) {
                    if(!Utils.checkCartItemforDownloadable(cartItemList))
                        callFragment(new BillingAndShippingAddressFragment(), "BillingAndShippingAddressFragment");
                    else
                       callFragment(new BillingAddressDownloadableFragment(), "BillingAddressDownloadableFragment");
                } else {
                    callFragment(new MyCheckOutFragment(), "MyCheckOutFragment");
                }*/
                getActivity().startActivity(new Intent(getActivity(), BillingAddress.class));
                break;
        }
    }

    @Override
    public void onResume() {
        Activity activity = getActivity();
        if(isAdded()&&activity!=null) {
           /* String headerText = getActivity().getResources().getString(R.string.cart_header);
            MainActivity_Home.setHeaderText(headerText);*/
        }
        super.onResume();
    }

    public  void deleteItemFromCartList(final int position) {
        if (cartItemList.size() > position) {
            final ProgressDialog pDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
            pDialog.setCancelable(true);
            pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pDialog.show();
            // close progress dialogbox after 1 second
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    removeItemFromCart(MyCartFragment.this.cartItemList.get(position), position);
                    setCounterItemAddedCart();
                    pDialog.dismiss();
                }
            }, 1000);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
       /* try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }*/
    }
}
