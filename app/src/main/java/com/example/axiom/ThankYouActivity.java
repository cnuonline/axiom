package com.example.axiom;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kloudportal.axiom.powertheater.R;

public class ThankYouActivity extends AppCompatActivity {
      Button continueShopping,logOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        continueShopping=(Button)findViewById(R.id.b_continue);
        logOut=(Button)findViewById(R.id.logout);

        continueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ThankYouActivity.this,MainActivity_Home.class));
                finish();
            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ThankYouActivity.this,Login_Activity.class));
                finish();
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 10 seconds
                startActivity(new Intent(ThankYouActivity.this,MainActivity_Home.class));
                finish();
            }
        }, 10000);
    }
}
