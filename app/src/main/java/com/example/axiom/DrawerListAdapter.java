package com.example.axiom;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kloudportal.axiom.powertheater.R;

import java.util.ArrayList;



class DrawerListAdapter extends BaseAdapter {
	 
    Context mContext;
    ArrayList<NavItem> mNavItems;
 
    public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
        mContext = context;
        mNavItems = navItems;
    }
 
    @Override
    public int getCount() {
        return mNavItems.size();
    }
 
    @Override
    public Object getItem(int position) {
        return mNavItems.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Typeface face = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drwer_item, null);
            face= Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
        }
        else {
            view = convertView;
        }
 
        TextView titleView = (TextView) view.findViewById(R.id.title);
        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
 
        titleView.setText( mNavItems.get(position).mTitle );
        titleView.setTypeface(face);
        iconView.setImageResource(mNavItems.get(position).mIcon);
 
        return view;
    }
}
