package com.example.axiom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.axiom.billing.UserManager;
import com.example.axiom.billing.UserProfileItem;
import com.example.axiom.billing.UserSession;
import com.kloudportal.axiom.powertheater.BuildConfig;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Register_Activity extends AppCompatActivity {
EditText adressStore, store_name, user_name, mobile_number, email_address, password, confirm_Password,secondarynumber;
TextView logintext;
String gstNumber;
Button registerbutton;

    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_);
      //  hideKeyboard(Register_Activity.this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        dialog = new ProgressDialog(Register_Activity.this,R.style.MyAlertDialogStyle);
                dialog.setMessage("Loading ... please wait.");
              //  dialog.show();
        store_name= (EditText)findViewById(R.id.store);
        store_name.setTypeface(face);
        user_name= (EditText)findViewById(R.id.name);
        user_name.setTypeface(face);
        secondarynumber=(EditText)findViewById(R.id.secondary_number);
        secondarynumber.setTypeface(face);
        mobile_number= (EditText)findViewById(R.id.phone);
        mobile_number.setTypeface(face);
        email_address= (EditText)findViewById(R.id.email_id);
        email_address.setTypeface(face);
        password= (EditText)findViewById(R.id.password);
        password.setTypeface(face);
        confirm_Password= (EditText)findViewById(R.id.confirm_password);
        adressStore= (EditText)findViewById(R.id.address);
        confirm_Password.setTypeface(face);
        adressStore.setTypeface(face);
        registerbutton= (Button) findViewById(R.id.register_button);
        registerbutton.setTypeface(face);
        logintext= (TextView) findViewById(R.id.login_text);
        logintext.setTypeface(face);
        logintext = (TextView) findViewById(R.id.login_text);

        URLInfoConstants.API_URL = BuildConfig.API_URL_MAP.get("test");
           Intent intent=getIntent();

              if(intent.hasExtra("storeName")){
                  store_name.setText(intent.getStringExtra("storeName"));
                  gstNumber=intent.getStringExtra("storeGstin");
                  adressStore.setText(intent.getStringExtra("storeAddress"));
              }



        logintext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register_Activity.this, Login_Activity.class));
            }
        });
        registerbutton = (Button) findViewById(R.id.register_button);
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
    }
    public void submitForm() {
        if (!ValidateEmail()) {
            return;
        } else if (!validateMobile()) {
            return;
        } else if (!validatePassword()) {
            return;
        } else {
            if (NetworkManager.isConnectToInternet(Register_Activity.this)) {
                checkpass();
            } else {
                ProjectUtils.showToast(Register_Activity.this, "No Internet Connection");
            }

        }
    }
     public void postCallRegister(){
        dialog.show();
             String url=URLInfoConstants.API_URL+"callback&store=1&service=createuser";
             //&firstname=Sony&password=dGVzdDEyMw==&email=sony@gmail.com&telephone=9988774455&gstin_no=36A&store_name=axiom&second_number=9988774455&store_address=KTC
             RequestQueue queue = Volley.newRequestQueue(this);
         StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                 new Response.Listener<String>()
                 {
                     @Override
                     public void onResponse(String response) {
                         // response
                         dialog.dismiss();
                         Log.d("Response", response);
                         try {
                             JSONObject jsonObject=new JSONObject(response);
                             if(jsonObject.getString("status").equalsIgnoreCase("1")){
                               String fsName=jsonObject.getString("firstname");
                               String lsName=jsonObject.getString("firstname");
                               String email=jsonObject.getString("email");
                               String telephone=jsonObject.getString("telephone");
                               String status=jsonObject.getString("status");
                               String idValue=jsonObject.getString("id");


                                 UserProfileItem user= new UserProfileItem(fsName, idValue, fsName, status, "",  email,"",telephone,"");

                                 /*get Active user*/
                                 UserProfileItem activeUser = UserSession.getActiveUser(Register_Activity.this);

                                 MyDataBaseAdapter dbAdapter = new MyDataBaseAdapter(Register_Activity.this);
                                 /*logout if user active*/
                                 if(activeUser!=null) {
                                     activeUser.setLogin_status("0");
                                     dbAdapter.updateOnlyUserProfileStatus(activeUser);
                                 }

                                 long insrtid= dbAdapter.inserUserProfiletData(user);

                                 /*set session*/
                                 UserSession.setSession(Register_Activity.this);
                                 UserManager.getInstance().setUser(user);



                                 startActivity(new Intent(Register_Activity.this,Login_Activity.class));
                             }else{
                                 Toast.makeText(Register_Activity.this,"User Already Exist",Toast.LENGTH_LONG).show();
                             }
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                     }
                 },
                 new Response.ErrorListener()
                 {
                     @Override
                     public void onErrorResponse(VolleyError error) {
                         // error
                         dialog.dismiss();
                         Log.d("Error.Response", error.toString());
                     }
                 }
         ) {

             @Override
             protected Map<String, String> getParams()
             {
                 Map<String, String> params = new HashMap<String, String>();
                 params.put("firstname", user_name.getText().toString().trim());
                 params.put("password", encodeToBase64(password.getText().toString().trim()));
                 params.put("email", email_address.getText().toString().trim());
                 params.put("telephone",mobile_number.getText().toString().trim());
                 params.put("gstin_no", gstNumber);
                 params.put("store_name", store_name.getText().toString().trim());
                 params.put("second_number", secondarynumber.getText().toString().trim());
                 params.put("store_address", adressStore.getText().toString().trim());

                 return params;
             }

         };

         queue.add(putRequest);
     }
    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(email_address.getText().toString().trim())) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean ValidateName() {
        if (!ProjectUtils.IsEditTextValidation(user_name)) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Name",Toast.LENGTH_LONG).show();

            return false;
        }
        return true;
    }

    public boolean validateMobile() {
        if (!ProjectUtils.IsMobleValidation(secondarynumber.getText().toString().trim())) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Mobile Number",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public boolean validateMobilePrimary() {
        if (!ProjectUtils.IsMobleValidation(mobile_number.getText().toString().trim())) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Mobile Number",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public boolean validatePassword() {
        if (password.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Password",Toast.LENGTH_LONG).show();

            return false;
        } else {
            if (!ProjectUtils.IsPasswordValidation(password.getText().toString().trim())) {
                Toast.makeText(Register_Activity.this,"Please Enter 6 Digit password",Toast.LENGTH_LONG).show();

                return false;
            } else {
                return true;
            }
        }
    }

    public void checkpass() {

        if (password.getText().toString().trim().equals("")) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid Password",Toast.LENGTH_LONG).show();
        } else if (password.getText().toString().trim().equals("")) {
            Toast.makeText(Register_Activity.this,"Please Enter Valid 6 digit password",Toast.LENGTH_LONG).show();
        } else if (!password.getText().toString().trim().equals(confirm_Password.getText().toString().trim())) {
            Toast.makeText(Register_Activity.this,"Please Enter Same Password",Toast.LENGTH_LONG).show();

        } else {
            if (NetworkManager.isConnectToInternet(Register_Activity.this)) {
              postCallRegister();
            } else {
                ProjectUtils.showToast(Register_Activity.this, "No Internet Connection");
            }
        }

    }
    public String encodeToBase64(String input) {
        byte[] data = new byte[0];
        try {
            data = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String base64Encoded = Base64.encodeToString(data, Base64.NO_WRAP);

        return base64Encoded;

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}



