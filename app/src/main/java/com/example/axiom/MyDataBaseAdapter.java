package com.example.axiom;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.axiom.AddCart.ShippingMethodItem;
import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.billing.UserProfileItem;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by kiran on 24/11/15.
 */
public class MyDataBaseAdapter extends SQLiteOpenHelper {
    // Database Name
    private static final String DATABASE_NAME = "cart_list_db";

    // Current version of database
    private static final int DATABASE_VERSION = 1;

   /* User profile table*/
    //table name
    private static final String TABLE_USER_PROFILE= "user_profile_table";
    // column name

    /*primary key*/
    private static final String KEY_USER_NAME = "username";//this is email id
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FIRST_NAME = "firstname";
    private static final String KEY_LAST_NAME = "lastname";
    private static final String KEY_USER_ID = "id";//this id provide by server
    private static final String KEY_LOGIN_STATUS = "login_status";
    private static final String SPECIALIZATION = "specialization";
    private static final String COMPNAY = "compnay";
    private static final String MOBILENUMBER = "mobileNumber";
    //create table
    private static final String CREATE_TABLE_USER="CREATE TABLE "
            + TABLE_USER_PROFILE + "("
            + KEY_USER_NAME + " TEXT  PRIMARY KEY,"
            + KEY_PASSWORD + " TEXT,"
            + KEY_FIRST_NAME + " TEXT,"
            + KEY_LAST_NAME + " TEXT,"
            + KEY_USER_ID + " TEXT,"
            + KEY_LOGIN_STATUS + " TEXT,"
            + SPECIALIZATION + " TEXT,"
            + COMPNAY + " TEXT,"
            + MOBILENUMBER + " TEXT );";

    private static final String TAG = "MyDataBaseAdapter";
    private static ShippingMethodItem shippingMethod = null;
    private static String coupon = null;
    private static float couponValue = 0.0f;

    public MyDataBaseAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        TAG.getClass().getSimpleName();
    }

    public MyDataBaseAdapter(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USER);

        onCreate(sqLiteDatabase);

    }

    public void SetShippingMethod(ShippingMethodItem shipMethod){
        shippingMethod = shipMethod;
    }

    public ShippingMethodItem getShippingMethod(){
        return shippingMethod;
    }

    public static void SetCoupon(String coupon, float couponValue){
        MyDataBaseAdapter.coupon = coupon;
        MyDataBaseAdapter.couponValue = couponValue;
    }

    public static String getCoupon(){
        return MyDataBaseAdapter.coupon;
    }

    public double getTotalPrice(){
        double totalPrice = ShoppingCart.getInstance().getSubTotal();

        if (shippingMethod != null){
            totalPrice += shippingMethod.getPrice();
        }

        if (coupon != null){
            totalPrice -= couponValue;
        }
        DecimalFormat precision = new DecimalFormat("0.00");
        totalPrice= Double.parseDouble(precision.format(totalPrice));

        return totalPrice;
    }

    /*insert data into user profile table*/
    public long inserUserProfiletData(UserProfileItem item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        Log.d(TAG, "inserUserProfiletData() called with: " + "primary key  = [" +  item.getUsername() + "]");
        ContentValues values = new ContentValues();

        values.put(KEY_FIRST_NAME, item.getFirstname());
        values.put(KEY_LAST_NAME, item.getLastname());
        values.put(KEY_USER_ID, item.getId());
        values.put(KEY_LOGIN_STATUS, item.getLogin_status());
        values.put(KEY_PASSWORD, item.getPassword());
        values.put(KEY_USER_NAME, item.getUsername());
        values.put(SPECIALIZATION,item.getSpecialization());
        values.put(COMPNAY,item.getCompnayName());
        values.put(MOBILENUMBER,item.getMobileNumber());


        return db.insert(TABLE_USER_PROFILE, null, values);
    }
    public int updateUserProfileData(UserProfileItem item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, item.getFirstname());
        values.put(KEY_LAST_NAME, item.getLastname());
        values.put(KEY_USER_ID, item.getId());
        values.put(KEY_LOGIN_STATUS, item.getLogin_status());
        values.put(KEY_PASSWORD, item.getPassword());
        //values.put(KEY_USER_NAME, item.getUsername());
        // update row in students table base on students.is value
        return db.update(TABLE_USER_PROFILE, values, KEY_USER_NAME + " = ?",
                new String[] { String.valueOf(item.getUsername()) });
    }
    public int updateOnlyUserProfileStatus(UserProfileItem item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_LOGIN_STATUS, item.getLogin_status());
        //values.put(KEY_USER_NAME, item.getUsername());
        // update row in students table base on students.is value
        Log.d(TAG, "updateOnlyUserProfileStatus() called with: " + "status = [" + item.getLogin_status() + "]");
        Log.d(TAG, "updateOnlyUserProfileStatus() called with: " + "user name = [" + item.getUsername() + "]");
        return db.update(TABLE_USER_PROFILE, values, KEY_USER_NAME + " = ?",
                new String[] { String.valueOf(item.getUsername()) });
    }
    public int updateOnlyUserProfilePssord(UserProfileItem item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_LOGIN_STATUS, item.getLogin_status());
        //values.put(KEY_USER_NAME, item.getUsername());
        // update row in students table base on students.is value
        Log.d(TAG, "updateOnlyUserProfilePssord() called with: " + "status = [" + item.getLogin_status() + "]");
        Log.d(TAG, "updateOnlyUserProfilePssord() called with: " + "user password = [" + item.getPassword() + "]");
        return db.update(TABLE_USER_PROFILE, values, KEY_PASSWORD + " = ?",
                new String[] { String.valueOf(item.getPassword()) });
    }
    public ArrayList<UserProfileItem> getUserProfile() {
        ArrayList<UserProfileItem> userProfile = new ArrayList<UserProfileItem>();

        String selectQuery = "SELECT  * FROM " + TABLE_USER_PROFILE;
        Log.d(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Log.d(TAG, "getUserProfile() called with:number of record  " +c.getCount()+ "");


        // looping through all rows and adding to list
        if (c.moveToFirst()) {

            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(0)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(1)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(2)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(3)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(4)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(5)+ "");
            do {

                //String firstname, String id, String lastname, String login_status, String password, String username
                UserProfileItem item = new UserProfileItem(c.getString(c.getColumnIndex(KEY_FIRST_NAME)),c.getString(c.getColumnIndex(KEY_USER_ID)),c.getString(c.getColumnIndex(KEY_LAST_NAME)),c.getString(c.getColumnIndex(KEY_LOGIN_STATUS)),c.getString(c.getColumnIndex(KEY_PASSWORD)),c.getString(c.getColumnIndex(KEY_USER_NAME)),c.getString(c.getColumnIndex(SPECIALIZATION)),c.getString(c.getColumnIndex(COMPNAY)),c.getString(c.getColumnIndex(MOBILENUMBER)));
                userProfile.add(item);
            } while (c.moveToNext());
        }

        db.close();
        return userProfile;
    }
    public boolean isUserAvailable(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        // SELECT * FROM students WHERE id = ?;
        String selectQuery = "SELECT  * FROM " + TABLE_USER_PROFILE + " WHERE " + KEY_USER_NAME + " = ? ";
        Log.d(TAG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, new String[]{id});



        if ( c.getCount()>0) {
            db.close();
            return true;
        }
        db.close();
        return false;
    }

}
