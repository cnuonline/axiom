package com.example.axiom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.load.model.Headers;
import com.kloudportal.axiom.powertheater.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registration1Activity extends AppCompatActivity {
    EditText gst_number,gst_Email;
//    TextView logintext;
    Button registerbutton;
    public static final String GSTINFORMAT_REGEX = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
    public static final String GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String storeName,storeAddress;
    String  gstinNumber;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration1);
     //  hideKeyboard(Registration1Activity.this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        gst_number= (EditText)findViewById(R.id.gst);
        gst_Email= (EditText)findViewById(R.id.gstemail);
        gst_number.setTypeface(face);
        gst_Email.setTypeface(face);
           progressDialog=new ProgressDialog(Registration1Activity.this,R.style.MyAlertDialogStyle);
                              progressDialog.setMessage("Loading...");
        registerbutton = (Button) findViewById(R.id.register1_button);
        registerbutton.setTypeface(face);
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             if(!TextUtils.isEmpty(gst_number.getText().toString())){
                    if(validateGSTIN(gst_number.getText().toString().trim())){

                       postCallLogin();

                    }else{
                        Toast.makeText(Registration1Activity.this,"GSTIN NOT VALID",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(Registration1Activity.this,"Please Enter GSTIN",Toast.LENGTH_LONG).show();

                }

            }
        });
    }

    public static boolean validateGSTIN(String gstin) {
        try {
            if (validGSTIN(gstin))
                return Boolean.TRUE;
            else
                return Boolean.FALSE;
        } catch (Exception e) {
            e.printStackTrace(); // Returns False.
        }
        return Boolean.FALSE; //Default False;
    }

    /**
     * Method to check if a GSTIN is valid. Checks the GSTIN format and the
     * check digit is valid for the passed input GSTIN
     *
     * @param gstin
     * @return boolean - valid or not
     * @throws Exception
     */
    private static boolean validGSTIN(String gstin) throws Exception {
        boolean isValidFormat = false;
        if (checkPattern(gstin, GSTINFORMAT_REGEX)) {
            isValidFormat = verifyCheckDigit(gstin);
        }
        return isValidFormat;

    }
    public void postCallLogin(){
        progressDialog.show();
           final String emailAddress=gst_Email.getText().toString().trim();
           String gstEmail ="online@axiomenergy.co.in";
        String  url = "https://api.mastergst.com/public/search?email="+gstEmail+"&gstin="+gst_number.getText().toString().trim();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONObject dataObject=jsonObject.getJSONObject("data");
                            storeName=dataObject.getString("lgnm");
                            gstinNumber=dataObject.getString("gstin");
                            JSONObject pradrObject=dataObject.getJSONObject("pradr");
                            JSONObject addressObject=pradrObject.getJSONObject("addr");
                            String bnm =addressObject.getString("bnm");
                            String st =addressObject.getString("st");
                            String loc =addressObject.getString("loc");
                            String bno =addressObject.getString("bno");
                            String dst =addressObject.getString("dst");
                            String stcd =addressObject.getString("stcd");
                            String city =addressObject.getString("city");
                            String flno =addressObject.getString("flno");
                            String pncd =addressObject.getString("pncd");
                            String lg =addressObject.getString("lg");
                            storeAddress=bnm+","+st+","+loc+","+bno+","+dst+","+stcd+","+city+","+flno+","+pncd+","+lg;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                       Intent intent =new Intent(Registration1Activity.this,Register_Activity.class);
                        intent.putExtra("storeName",storeName);
                        intent.putExtra("storeAddress",storeAddress);
                        intent.putExtra("storeGstin",gstinNumber);
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                          progressDialog.dismiss();
                        Log.d("Error.Response", error.toString());
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                    }
                }
        ) {

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("client_id", " l7xx2d959ff3553749139ae17d471932b39f");
                params.put("client_secret", "5509512fab3b4fd48075f9714d0aa7ca");

                return params;
            }


            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();



                return params;
            }

        };

        queue.add(putRequest);
    }








    /**
     * Method for checkDigit verification.
     *
     * @param gstinWCheckDigit
     * @return
     * @throws Exception
     */
    private static boolean verifyCheckDigit(String gstinWCheckDigit) throws Exception {
        Boolean isCDValid = false;
        String newGstninWCheckDigit = getGSTINWithCheckDigit(
                gstinWCheckDigit.substring(0, gstinWCheckDigit.length() - 1));

        if (gstinWCheckDigit.trim().equals(newGstninWCheckDigit)) {
            isCDValid = true;
        }
        return isCDValid;
    }

    /**
     * Method to check if an input string matches the regex pattern passed
     *
     * @param inputval
     * @param regxpatrn
     * @return boolean
     */
    private static boolean checkPattern(String inputval, String regxpatrn) {
        boolean result = false;
        if ((inputval.trim()).matches(regxpatrn)) {
            result = true;
        }
        return result;
    }

    /**
     * Method to get the check digit for the gstin (without checkdigit)
     *
     * @param gstinWOCheckDigit
     * @return : GSTIN with check digit
     * @throws Exception
     */
    private static String getGSTINWithCheckDigit(String gstinWOCheckDigit) throws Exception {
        int factor = 2;
        int sum = 0;
        int checkCodePoint = 0;
        char[] cpChars;
        char[] inputChars;

        try {
            if (gstinWOCheckDigit == null) {
                throw new Exception("GSTIN supplied for checkdigit calculation is null");
            }
            cpChars = GSTN_CODEPOINT_CHARS.toCharArray();
            inputChars = gstinWOCheckDigit.trim().toUpperCase().toCharArray();

            int mod = cpChars.length;
            for (int i = inputChars.length - 1; i >= 0; i--) {
                int codePoint = -1;
                for (int j = 0; j < cpChars.length; j++) {
                    if (cpChars[j] == inputChars[i]) {
                        codePoint = j;
                    }
                }
                int digit = factor * codePoint;
                factor = (factor == 2) ? 1 : 2;
                digit = (digit / mod) + (digit % mod);
                sum += digit;
            }
            checkCodePoint = (mod - (sum % mod)) % mod;
            return gstinWOCheckDigit + cpChars[checkCodePoint];
        } finally {
            inputChars = null;
            cpChars = null;
        }
    }

    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(gst_Email.getText().toString().trim())) {
            Toast.makeText(Registration1Activity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
