package com.example.axiom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kloudportal.axiom.powertheater.BuildConfig;

import com.kloudportal.axiom.powertheater.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Login_Activity extends AppCompatActivity {
    EditText email_id, Password;
    Button loginbutton;
    TextView registertext,forgot;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
            // hideKeyboard(Login_Activity.this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
       /* View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
         Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        dialog = new ProgressDialog(Login_Activity.this,R.style.MyAlertDialogStyle);
        dialog.setMessage("Loading ...");
        email_id= (EditText)findViewById(R.id.email);
        email_id.setTypeface(face);
        Password= (EditText)findViewById(R.id.password);
        Password.setTypeface(face);
        loginbutton= (Button) findViewById(R.id.login_button);
        loginbutton.setTypeface(face);
        registertext= (TextView) findViewById(R.id.register_text);
        registertext.setTypeface(face);
        forgot= (TextView) findViewById(R.id.forgotpassword);
        forgot.setTypeface(face);
      /*  email_id.setText("online@axiomenergy.co.in");
        Password.setText("kiranraji");*/

           SharedPreferences sharedPreferences=getSharedPreferences("LoginPref", MODE_PRIVATE);

        if (sharedPreferences.contains("isFirstRun")) {
            Boolean isFirstRun = sharedPreferences.getBoolean("isFirstRun", false);

            if (isFirstRun) {
                startActivity(new Intent(Login_Activity.this, MainActivity_Home.class));
                finish();
            }

        }

        loginbutton = (Button) findViewById(R.id.login_button);
        URLInfoConstants.API_URL = BuildConfig.API_URL_MAP.get("test");
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(!TextUtils.isEmpty(Password.getText().toString().trim())){
                   submitForm();
               }else {
                   Toast.makeText(Login_Activity.this,"Please Enter Correct Username and Password",Toast.LENGTH_LONG).show();
               }

            }
        });

        registertext = (TextView) findViewById(R.id.register_text);
        registertext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login_Activity.this, Registration1Activity.class));
            }
        });
        forgot = (TextView) findViewById(R.id.forgotpassword);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login_Activity.this, Forgot_Activity.class));
            }
        });






    }
    public void submitForm() {
        if (!ValidateEmail()) {
            return;
        }  else {
            if (NetworkManager.isConnectToInternet(Login_Activity.this)) {
                postCallLogin();
            } else {
                ProjectUtils.showToast(Login_Activity.this, "No Internet Connection");
            }

        }
    }
    public void postCallLogin(){
        dialog.show();
        SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
        final SharedPreferences.Editor editor=sharedPreferences.edit();
        String  url = URLInfoConstants.API_URL+"callback&store=1&service=verifylogin";
        //&email=sony.isha143@gmail.com&password=dGVzdDEyMw==
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getString("login_status").equalsIgnoreCase("1")){
                                startActivity(new Intent(Login_Activity.this, MainActivity_Home.class));
                                finish();
                                editor.putString("email",jsonObject.getString("email"));
                                editor.putString("user_id",jsonObject.getString("id"));
                                editor.putString("firstname",jsonObject.getString("firstname"));
                                editor.putString("gstin_no",jsonObject.getString("gstin_no"));
                                editor.putString("store_name",jsonObject.getString("store_name"));
                                editor.apply();
                                editor.commit();
                                SharedPreferences sharedPreferenceslogin = getSharedPreferences("LoginPref", MODE_PRIVATE);
                                SharedPreferences.Editor editorlogin = sharedPreferenceslogin.edit();
                                editorlogin.putBoolean("isFirstRun", true);
                                editorlogin.commit();
                                editorlogin.apply();

                            }else if(jsonObject.getString("login_status").equalsIgnoreCase("0")){
                                Toast.makeText(Login_Activity.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("password", encodeToBase64(Password.getText().toString().trim()));
                params.put("email", email_id.getText().toString().trim());


                return params;
            }

        };

        queue.add(putRequest);
    }
    public String encodeToBase64(String input) {
        byte[] data = new byte[0];
        try {
            data = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String base64Encoded = Base64.encodeToString(data, Base64.NO_WRAP);

        return base64Encoded;

    }
    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(email_id.getText().toString().trim())) {
            Toast.makeText(Login_Activity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    static boolean emailValidate(String target) {
//        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
//        return matcher.find();
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
            view.clearFocus();
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
