package com.example.axiom.AddCart;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;


import com.kloudportal.axiom.powertheater.R;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Currency;

/**
 * Created by manish on 22/01/16.
 */
public class Utils {



    public static String appendWithCurrencySymbol(double value){
        //Replace it with number format
        Currency currency = Currency.getInstance(Config.getInstance().getCurrencyCode());
        DecimalFormat decimalFormat = new DecimalFormat("#.00");

//        return currency.getSymbol() + " "+ decimalFormat.format(value);
        return Config.getInstance().getCurrency_symbol() + " "+ decimalFormat.format(value);
    }

    public static String getAppName(Context context){

        return context.getResources().getString(R.string.app_name);
    }

    public static String encodeToBase64(String input) {
        byte[] data = new byte[0];
        try {
            data = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String base64Encoded = Base64.encodeToString(data, Base64.DEFAULT);

        return base64Encoded;

    }

    public static boolean checkCartItemforDownloadable(ArrayList<ShoppingCartItem> cartItemList) {
        for (int i = 0; i <cartItemList.size() ; i++) {
            if (cartItemList.get(i).getShoppingItem().getType().toString().equals("downloadable")) {
                return true;
            }
        }
        return false;
    }

}
