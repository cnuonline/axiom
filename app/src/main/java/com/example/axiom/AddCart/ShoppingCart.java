package com.example.axiom.AddCart;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by manish on 08/03/16.
 */
public class ShoppingCart {
    private static ShoppingCart ourInstance = new ShoppingCart();

    public static ShoppingCart getInstance() {
        return ourInstance;
    }

    private HashMap<String, ShoppingCartItem> cart = new HashMap<>();
    private String coupon;

    private ShoppingCart() {
    }

    public void addItem(ShoppingCartItem item){
        this.cart.put(item.getShoppingItem().getId(), item);
    }

    public void deleteItem(ShoppingCartItem item){
        this.cart.remove(item.getShoppingItem().getId());

        if(this.cart.isEmpty()){
            this.coupon = null;
        }
    }

    public int getCount(String id){
        int count = 0;

        if(this.cart.containsKey(id)){
            count = this.cart.get(id).getCount();
        }

        return count;
    }

    public int getCount(ShoppingItem item){
        return this.getCount(item.getId());
    }

    public int getNumDifferentItems(){
        return this.cart.size();
    }

    public double getSubTotal(ShoppingItem item){
        return getCount(item) * item.getFinalPrice();
    }

    public double getSubTotal(){
        double subTotal = 0.0;

        Set<String> itemIds = this.cart.keySet();
        for(String id : itemIds){
            ShoppingItem item = this.cart.get(id).getShoppingItem();
            subTotal += this.getSubTotal(item);
        }

        return subTotal;
    }

    public Collection<ShoppingCartItem> getCartItems(){
        return this.cart.values();
    }

    public void clearCart(){
        this.cart.clear();;
    }
}
