package com.example.axiom.AddCart;

import android.support.annotation.Nullable;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by manish on 20/01/16.
 */
public class ShoppingItem extends Entity{
    private int maxAllwoedQuantity;

    @Override
    public String getId() {
        return id;
    }

    private final String id;
    private final String name;
    private final String sku;
    private String price;
    private final String specialPrice;
    private final String inStock;
    private final String stockQuantity;
    private final String type;
    private final String image;
    private String thumbnail="";
    private ArrayList<String> images;

    @Nullable
    public static ShoppingItem create(JSONObject jsonObject){
        ShoppingItem item = null;
        try {
            String id = jsonObject.getString("id");
            String name = jsonObject.getString("name");
            String type = jsonObject.getString("type");
            String image = jsonObject.getString("image");
            String price = jsonObject.getString("price");
            String specialPrice  = jsonObject.getString("special_price");
            String isStockStatus = jsonObject.getString("is_stock_status");
            int maxAllQuantity=10;
            if(jsonObject.has("max_allowed_quantity")) {
                maxAllQuantity = jsonObject.getInt("max_allowed_quantity");
            }

            item = new ShoppingItem(id, name, "sku", image, price, specialPrice, isStockStatus, "1", type,maxAllQuantity);

            ShoppingItemManager.getInstance().addShoppingItem(item);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return item;
    }

    public ShoppingItem(String id, String name, String sku, String image, String price, String specialPrice, String inStock, String stockQuantity, String type, String thumbnail,int maxAllQuantity){
        super(id);
        this.id=id;
        this.name = name;
        this.sku = sku;
        this.image = image;
        this.price = price;
        this.specialPrice = specialPrice;
        this.inStock = inStock;
        this.stockQuantity = stockQuantity;
        this.type = type;
        this.thumbnail= thumbnail;
        this.maxAllwoedQuantity=maxAllQuantity;
    }

    public ShoppingItem(String id, String name, String sku, String image, String price, String specialPrice, String inStock, String stockQuantity, String type, int maxAllQuantity){
        super(id);
        this.id=id;
        this.name = name;
        this.sku = sku;
        this.image = image;
        this.price = price;
        this.specialPrice = specialPrice;
        this.inStock = inStock;
        this.stockQuantity = stockQuantity;
        this.type = type;
        this.maxAllwoedQuantity=maxAllQuantity;
    }

    public String getName(){
        return this.name;
    }
    public String getImage() {
        if(this.images != null && this.images.size() > 0){
            return this.images.get(0);
        }else{
            return this.image;
        }
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getPriceStr(){return this.price;}
    public String getSpecialPriceStr(){return this.specialPrice;}
    public double getPrice(){
        price=price.replace(",","");
        return Double.parseDouble(this.price);}
    public double getSpecialPrice(){return Double.parseDouble(this.specialPrice);}

    public double getFinalPrice(){
        double finalPrice  = getPrice();
        double spPrice = getSpecialPrice() ;

        if(spPrice > 0.0 && finalPrice > spPrice){
            finalPrice = spPrice;
        }

        return finalPrice;
    }

    public String getFinalPriceWithCurrency(){
        return Utils.appendWithCurrencySymbol(this.getFinalPrice());
    }

    public String getInStock(){return this.inStock;}
    public String getStock(){return this.stockQuantity;}

    public boolean isInStock(){
        boolean haveStock = this.inStock.equals("1");
        haveStock |= getStockQuantity() > 0;

        return haveStock;
    }

    public int getStockQuantity(){
        return (int)Float.parseFloat(this.stockQuantity);
    }

    public String getSku(){
        return this.sku;
    }
    public String getType() { return this.type;}

    public JSONObject createJSON(int quantity){
        HashMap<String, String> productMap = this.createJSONMAP(quantity);
        JSONObject jsnObj = new JSONObject(productMap);

        return jsnObj;
    }

    public void setImageList(ArrayList<String> imageList){
        this.images = imageList;
    }

    protected HashMap<String, String> createJSONMAP(int quantity){
        HashMap<String, String> productMap = new HashMap<String, String>();
        productMap.put("id", this.getId());
        if(!this.getType().toString().equals("downloadable"))
            productMap.put("type", this.getType());

        productMap.put("sku", this.getSku());
        productMap.put("quantity", String.valueOf(quantity));

        return productMap;
    }

    public int getmaxAllwoedQuantity() {
        return maxAllwoedQuantity;
    }
}