package com.example.axiom.AddCart;

/**
 * Created by kiran on 23/11/15.
 */
public class ShoppingCartItem {
    private final ShoppingItem shoppingItem;
    private final int count;

    public ShoppingCartItem(ShoppingItem item, int count){
        this.shoppingItem = item;
        this.count = count;
    }

    public ShoppingItem getShoppingItem(){
        return this.shoppingItem;
    }

    public int getCount(){
        return this.count;
    }
}