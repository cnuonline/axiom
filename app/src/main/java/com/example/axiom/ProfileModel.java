package com.example.axiom;

public class ProfileModel {

      String storeName;
      String personName;
      String sEmail;
      String storeGst;
      String primaryNumber;
      String secoundaryNumber;
      String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getStoreGst() {
        return storeGst;
    }

    public void setStoreGst(String storeGst) {
        this.storeGst = storeGst;
    }

    public String getPrimaryNumber() {
        return primaryNumber;
    }

    public void setPrimaryNumber(String primaryNumber) {
        this.primaryNumber = primaryNumber;
    }

    public String getSecoundaryNumber() {
        return secoundaryNumber;
    }

    public void setSecoundaryNumber(String secoundaryNumber) {
        this.secoundaryNumber = secoundaryNumber;
    }
}
