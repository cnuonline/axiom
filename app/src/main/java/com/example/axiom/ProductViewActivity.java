package com.example.axiom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.axiom.Adapter.BannerAdapterProductDetails;
import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.AddCart.ShoppingCartItem;
import com.example.axiom.AddCart.ShoppingItem;
import com.example.axiom.AddCart.ShoppingItemManager;
import com.example.axiom.AddCart.SimpleShoppingItem;
import com.example.axiom.utube.Constants;
import com.kloudportal.axiom.powertheater.R;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class ProductViewActivity extends AppCompatActivity {

    private Toolbar toolbar;
    Button buynowbut,cartbut;
    TextView toolbartext, product_name,counttext,pricetext,stocktext,desctext,desctheory;
    ViewPager mPager;
    CirclePageIndicator indicator;
     Handler handler;
    private int slideImgCounter;
    private boolean reverseFlag;
    private ArrayList<BannerItem> bannerListData;
    private BannerAdapterProductDetails adapter;

    //intilise
    private static final int numberOfItem = 1;
    public static ArrayList<AdditionalInfoItem> additionalInfoList;
    public static String cartImage;
    private String TAG = "";
    private String PRODUCT_ID;
    private String PRODUCT_NAME = "";
    private String type;
    private String has_custom_option,description;
    private ArrayList<ShoppingCartItem> cartItemList;
    private TextView txtV_price;
    private TextView txtV_description;
    private String short_description;
    private ShoppingItem currentItem = null;
    private ShoppingItem reorderItem;
    private ArrayList<String> imgListStrArry;
    ProgressDialog progressDialog;
    WebView webView;
    TextView featuretag,specificationtag,spectheory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        progressDialog=new ProgressDialog(ProductViewActivity.this,R.style.MyAlertDialogStyle);
                            progressDialog.setMessage("please wait Loading....");
        imgListStrArry = new ArrayList<>();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Product View");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        product_name=(TextView)findViewById(R.id.productname);
        counttext=(TextView)findViewById(R.id.count);
        pricetext=(TextView)findViewById(R.id.pricetext);
        stocktext=(TextView) findViewById(R.id.stock);
        desctext=(TextView) findViewById(R.id.desc);
        desctheory=(TextView) findViewById(R.id.theory);
        webView = (WebView) findViewById(R.id.webView1);
        buynowbut=(Button) findViewById(R.id.buynow);
        cartbut=(Button) findViewById(R.id.cart);
        featuretag=(TextView)findViewById(R.id.featuretag);
        specificationtag=(TextView)findViewById(R.id.specificationtag);
        spectheory=(TextView)findViewById(R.id.theoryspecification);
        product_name.setTypeface(face);
        counttext.setTypeface(face);
        pricetext.setTypeface(face);
        stocktext.setTypeface(face);
        desctext.setTypeface(face);
        buynowbut.setTypeface(face);
        desctheory.setTypeface(face);
        spectheory.setTypeface(face);
        cartbut.setTypeface(face);
        toolbartext.setTypeface(face);
        mPager = (ViewPager)findViewById(R.id.pager);
        indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        handler = new Handler();
        bannerListData = new ArrayList<>();
       /* desctheory.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        desctext.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        spectheory.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);*/

        Intent intent=getIntent();
        if(intent.hasExtra("productid")){
            PRODUCT_ID=intent.getStringExtra("productid");
            PRODUCT_NAME=intent.getStringExtra("productname");
        }
        if(PRODUCT_ID.equalsIgnoreCase("1")){
            cartbut.setVisibility(View.VISIBLE);
            buynowbut.setVisibility(View.VISIBLE);

        }else{
            cartbut.setVisibility(View.GONE);
            buynowbut.setVisibility(View.GONE);
        }
        getProductViewDetails(PRODUCT_ID);
        getProductDescription(PRODUCT_ID);
        getImagesUrl(PRODUCT_ID);

       // moveBannerImages();

        cartbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 addItemToCart();
            }
        });
        buynowbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemToCart();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void moveBannerImages() {
        Log.d("HomeFragment", "Enter in Thread Handler");
        Runnable runnable = new Runnable() {

            public void run() {
                Log.d("HomeFragment", "Running in Thread Handler");
                if (slideImgCounter == bannerListData.size() - 1)
                    reverseFlag = true;
                if (slideImgCounter == 0)
                    reverseFlag = false;


                mPager.setCurrentItem(slideImgCounter, true);
                //indicator.setViewPager(mPager);
                if (reverseFlag)
                    slideImgCounter--;
                else
                    slideImgCounter++;


                handler.postDelayed(this, 5000);
            }
        };
        runnable.run();
    }


       public void getProductViewDetails(String idValue){
             progressDialog.show();
           final String url = URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=productdetaildescription&productid=" + idValue;

           StringRequest stringRequest =new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
               @Override
               public void onResponse(String response) {
                      Log.d("reponseproductdetails",response);
                      if(progressDialog.isShowing())
                          progressDialog.dismiss();
                   BannerItem bannerItem=new BannerItem();


                   try {
                       JSONObject strJSNobj = new JSONObject(response);
                       String name = strJSNobj.getString("name");
                       String quantity = strJSNobj.getString("quantity");
                       String price = strJSNobj.getString("price");
                       String sprice = strJSNobj.getString("sprice");
                       String sku = strJSNobj.getString("sku");
                       description = strJSNobj.getString("description");
                       short_description = strJSNobj.getString("shortdes");
                       int maxAllQuantity=10;
                       if(strJSNobj.has("max_allowed_quantity")) {
                           maxAllQuantity = strJSNobj.getInt("max_allowed_quantity");
                       }
                       cartImage = strJSNobj.getString("img");
                       String url = strJSNobj.getString("url");
                       Log.d("onlyurl ", url);
                       type = strJSNobj.getString("type");

                       has_custom_option = strJSNobj.getString("has_custom_option");

                       if(cartImage==null)
                           cartImage="";
                       if (!Validation.isNull(PRODUCT_ID, name, sku, cartImage, price, sprice, quantity)) {
                           if(!type.equals("downloadable"))
                           {
                               currentItem = new SimpleShoppingItem(PRODUCT_ID, name, sku, cartImage, price, sprice, "1", quantity, type, url,maxAllQuantity);// edited by prashant
                               ShoppingItemManager.getInstance().addShoppingItem(currentItem);
                           }
                           product_name.setText(name);
                           if(PRODUCT_ID.equalsIgnoreCase("1")){
                               counttext.setText("\u20b9"+price);
                           }else{
                               counttext.setText("COMING SOON");
                           }

                           if(description != null){
                              //desctheory.setText(description.replace("\r"," "));
                               desctheory.setText(Html.fromHtml(description.replace("\r\n"," "), null, new MyTagHandler()));


                           }else{
                               featuretag.setVisibility(View.GONE);
                               desctheory.setVisibility(View.GONE);
                           }
                           if(short_description.equals("")){
                               specificationtag.setVisibility(View.GONE);
                               spectheory.setVisibility(View.GONE);

                           }else{
                               specificationtag.setVisibility(View.VISIBLE);
                               spectheory.setVisibility(View.VISIBLE);
                               spectheory.setText(Html.fromHtml(short_description.replace("\r\n"," ")));
                           }



                           //justify(desctheory);

                       }
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

                   }
           }, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {
                   if(progressDialog.isShowing())
                       progressDialog.dismiss();
                   Log.d("erroreproductdetails",error.toString());
               }
           });

           Volley.newRequestQueue(ProductViewActivity.this).add(stringRequest);


           }


            public  void getProductDescription(String valueId){
                   String url= URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=product_description&productid="+valueId;
                   StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                       @Override
                       public void onResponse(String response) {
                                Log.d("response",response);
                           try {
                               JSONObject jsonObject=new JSONObject(response);
                               desctext.setText(jsonObject.getString("description"));
                               webView.loadData(jsonObject.getString("description").replaceAll("\r\n"," "), "text/html", "utf-8");
                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }
                   }, new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                                 Log.d("error",error.toString());
                       }
                   });
                   Volley.newRequestQueue(ProductViewActivity.this).add(stringRequest);
            }

            public void getImagesUrl(String idValue){
               progressDialog.show();
              String url=URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=productdetailimage&productid="+idValue;
              StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                  @Override
                  public void onResponse(String response) {
                       Log.d("responseImage",response.toString());
                      if(progressDialog.isShowing())
                          progressDialog.dismiss();
                      try {
                          JSONObject strJSNobj = new JSONObject(response);
                          JSONArray imageJSNArray = strJSNobj.getJSONArray("image");
                          imgListStrArry = new ArrayList<>();

                          for (int i = 0; i < imageJSNArray.length(); i++) {

                              imgListStrArry.add(imageJSNArray.getString(i));
                          }
                          ShoppingItem item = ShoppingItemManager.getInstance().getShoppingItem(PRODUCT_ID);

                          if (item != null) {
                              // put null checker
                              item.setImageList(imgListStrArry);
                          }

                      } catch (JSONException e) {
                          e.printStackTrace();
                      }
                        adapter = new BannerAdapterProductDetails(ProductViewActivity.this, imgListStrArry);
                           mPager.setAdapter(adapter);
                           mPager.setCurrentItem(0, true);

                           CirclePageIndicator mIndicator = indicator;
                           indicator.setViewPager(mPager);
                           Activity activity = ProductViewActivity.this;
                           if (activity != null) {
                               final float density = getResources().getDisplayMetrics().density;

                               indicator.setRadius(3.2f * density);
                           }
                           indicator.setPageColor(0xFFCBCBCB);
                           indicator.setFillColor(Color.BLACK);
                           indicator.setStrokeColor(0xFFCBCBCB);
                           indicator.setSnap(true);
                           indicator.setStrokeWidth(1);
                  }
              }, new Response.ErrorListener() {
                  @Override
                  public void onErrorResponse(VolleyError error) {
                      if(progressDialog.isShowing())
                          progressDialog.dismiss();
                      Log.d("errorImage",error.toString());
                  }
              });

               Volley.newRequestQueue(getApplicationContext()).add(stringRequest);




           }




    private void addItemToCart() {
        if (currentItem != null || reorderItem != null) {
            ShoppingCartItem item;
            if (currentItem != null) {
                item = new ShoppingCartItem(currentItem, numberOfItem);
            } else {
                item = new ShoppingCartItem(reorderItem, numberOfItem);
            }
            //test

            Collection<ShoppingCartItem> sp = ShoppingCart.getInstance().getCartItems();
            int numofcartitems = sp.size();
            if (numofcartitems != 0) {
                int FLAG = 0;
                for (int i = 0; i < sp.size(); i++) {
                    Iterator<ShoppingCartItem> it = sp.iterator();
                    while (it.hasNext()) {
                        ShoppingCartItem sci = it.next();
                        if (currentItem != null) {
                            if (sci.getShoppingItem().getId().equals(currentItem.getId())) {
                                Activity activity = ProductViewActivity.this;
                                if(activity!=null) {
                                    final Toast toast = Toast.makeText(activity, "Item already added", Toast.LENGTH_SHORT);
                                    toast.show();

                                }
                                FLAG = 0;
                                break;
                            } else {
                                FLAG = 1;
                            }
                        } else {
                            if (sci.getShoppingItem().getId().equals(reorderItem.getId())) {
                                FLAG = 0;
                                break;
                            } else {
                                FLAG = 1;
                            }
                        }
                    }

                }

                if (FLAG == 1) {
                    ShoppingCart.getInstance().addItem(item);
                }
            } else {
                ShoppingCart.getInstance().addItem(item);
            }

            //end test
            cartItemList = new ArrayList<>(ShoppingCart.getInstance().getCartItems());

            if (cartItemList.size() > 0) {
               /* MainActivity_Home.txtV_item_counter.setText(cartItemList.size() + "");
                MainActivity_Home.txtV_item_counter.setText(cartItemList.size() + "");
                MainActivity_Home.txtV_item_counter.setVisibility(View.VISIBLE);*/
            } else {
                /*MainActivity_Home.txtV_item_counter.setVisibility(View.INVISIBLE);*/
            }
            //  new MyCartFragment().setCounterItemAddedCart(cartItemList);
            // callFragment(new MyCartFragment());
            startActivity(new Intent(ProductViewActivity.this,AddToCartActivity.class));
        }
    }

    public  void justify(final TextView textView) {

        final AtomicBoolean isJustify = new AtomicBoolean(false);

        final String textString = short_description;

        final TextPaint textPaint = textView.getPaint();

        final SpannableStringBuilder builder = new SpannableStringBuilder();

        textView.post(new Runnable() {
            @Override
            public void run() {

                if (!isJustify.get()) {

                    final int lineCount = textView.getLineCount();
                    final int textViewWidth = textView.getWidth();

                    for (int i = 0; i < lineCount; i++) {

                        int lineStart = textView.getLayout().getLineStart(i);
                        int lineEnd = textView.getLayout().getLineEnd(i);

                        String lineString = textString.substring(lineStart, lineEnd);

                        if (i == lineCount - 1) {
                            builder.append(new SpannableString(lineString));
                            break;
                        }

                        String trimSpaceText = lineString.trim();
                        String removeSpaceText = lineString.replaceAll(" ", "");

                        float removeSpaceWidth = textPaint.measureText(removeSpaceText);
                        float spaceCount = trimSpaceText.length() - removeSpaceText.length();

                        float eachSpaceWidth = (textViewWidth - removeSpaceWidth) / spaceCount;

                        SpannableString spannableString = new SpannableString(lineString);
                        for (int j = 0; j < trimSpaceText.length(); j++) {
                            char c = trimSpaceText.charAt(j);
                            if (c == ' ') {
                                Drawable drawable = new ColorDrawable(0x00ffffff);
                                drawable.setBounds(0, 0, (int) eachSpaceWidth, 0);
                                ImageSpan span = new ImageSpan(drawable);
                                spannableString.setSpan(span, j, j + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                        }

                        builder.append(spannableString);
                    }

                    textView.setText(builder);
                    isJustify.set(true);
                }
            }
        });
    }
}
