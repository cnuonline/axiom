package com.example.axiom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.billing.AddressData;
import com.example.axiom.billing.UserManager;
import com.example.axiom.billing.UserProfileItem;
import com.kloudportal.axiom.powertheater.R;

import org.json.JSONException;
import org.json.JSONObject;


public class OrderAcknowledgement extends AppCompatActivity {
    TextView orderIdvalue,name,shippingAddress,city,state,country,pincode,amount,phonenumber;
    String customerIdValue;
    ProgressDialog progressDialog;
    Button continueshopping;
    private Toolbar toolbar;
    TextView toolbartext,cgstvalue,sgstvalue,subamount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_acknowledgement);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Order Acknowledgement");
        toolbartext.setTypeface(face);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog=new ProgressDialog(OrderAcknowledgement.this,R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Loading...");
         orderIdvalue=(TextView) findViewById(R.id.txtV_order_id_value);
         name=(TextView)findViewById(R.id.ttxtV_label__name_value);
         shippingAddress=(TextView)findViewById(R.id.txtV_lshipp_add_value);
         city=(TextView)findViewById(R.id.txtV_lshipp_city_value);
         state=(TextView)findViewById(R.id.txtV_lshipp_state_value);
         country=(TextView)findViewById(R.id.txtV_lshipp_country_value);
         pincode=(TextView)findViewById(R.id.txtV_lshipp_zipcode_value);
         amount=(TextView)findViewById(R.id.txtV_lshipp_payable_value);
         phonenumber=(TextView)findViewById(R.id.txtV_lshipp_contact_value);
         cgstvalue=(TextView)findViewById(R.id.txtV_lshipp_cgst_add_value);
         sgstvalue=(TextView)findViewById(R.id.txtV_lshipp_sgst_add_value);
        subamount=(TextView)findViewById(R.id.txtV_lshipp_subamountpayable_value);
        SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
           getBillingAddress(customerIdValue);
           // double pro_total_price=  ShoppingCart.getInstance().getSubTotal();

           Intent intent=getIntent();
           if(intent.hasExtra("orderid")){
               orderIdvalue.setText(intent.getStringExtra("orderid"));
               amount.setText("\u20b9"+intent.getStringExtra("total"));
               cgstvalue.setText("\u20b9"+Integer.parseInt(intent.getStringExtra("total"))%2);
               sgstvalue.setText("\u20b9"+Integer.parseInt(intent.getStringExtra("total"))%2);
               subamount.setText("\u20b9"+(intent.getStringExtra("subtotal")));
           }

          continueshopping=(Button)findViewById(R.id.continue_shopping_button);
          continueshopping.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  startActivity(new Intent(OrderAcknowledgement.this,ThankYouActivity.class));
                  finish();
              }
          });
    }

    public void getBillingAddress(String idValue){
        progressDialog.show();
        String url =URLInfoConstants.API_URL+"callback&service=get_address&customer_id="+idValue;

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
                JSONObject strJSNobj = null;

                try {
                    strJSNobj = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    AddressData addressData = null;

                    JSONObject jsonObject = strJSNobj.getJSONObject("billing_address");
                    try {
                        String firstNames = jsonObject.getString("firstname");
                        String lastNames = jsonObject.getString("firstname");
                        String contactNos = jsonObject.getString("telephone");
                        String citys = jsonObject.getString("city");
                        String states = jsonObject.getString("street");
                        String streets = jsonObject.getString("region");
                        String countryIds = jsonObject.getString("country_id");
                        String pinCodes = jsonObject.getString("postcode");
                        name.setText(firstNames);
                        phonenumber.setText(contactNos);
                        city.setText(citys);
                        state.setText(states);
                        if(streets .equalsIgnoreCase("null")){
                            shippingAddress.setHint("Address");

                        }else{
                            shippingAddress.setText(streets);
                        }
                        if(pinCodes.equalsIgnoreCase("000000")){
                            pincode.setHint("Pincode");

                        }else{
                            pincode.setText(pinCodes);

                        }
                        country.setText(countryIds);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getBillingAddress(strJSNobj.getString("id"));
                    AddressData addressDatas = AddressData.create(jsonObject);
                    UserProfileItem activeUser = UserManager.getInstance().getUser();
                    if (activeUser != null) {
                        activeUser.setBillingAddress(addressDatas);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });

        Volley.newRequestQueue(OrderAcknowledgement.this).add(stringRequest);



    }
    @Override
    public boolean onSupportNavigateUp() {

        return true;
    }
}
