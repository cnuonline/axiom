package com.example.axiom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
    private Toolbar toolbar;
    TextView toolbartext, gstntext,gstnvalue,storetext,storevalue,nametext,phonetext,emailtext,addresstxt,addressval;
    TextView phonetexts;
    EditText namevalue, phonevalue,emailvalue,phonevalues;
    ProfileModel profileModel;
    TextView passText;
    EditText address;
    ProgressDialog progressDialog;
    Button editButton;
    String customerIdValue="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        progressDialog=new ProgressDialog(EditProfileActivity.this,R.style.MyAlertDialogStyle);
                           progressDialog.setMessage("Please wait Loading...");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Edit Profile");
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        profileModel=PersistanceForProfile.getInstance().getProfileModel();
        gstntext=(TextView)findViewById(R.id.gst);
        gstnvalue=(TextView)findViewById(R.id.gstn);
        storetext=(TextView)findViewById(R.id.storetext);
        storevalue=(TextView)findViewById(R.id.store);
        nametext=(TextView)findViewById(R.id.nametext);
        namevalue=(EditText) findViewById(R.id.name);
        phonetext=(TextView)findViewById(R.id.phonetext);
        phonetexts=(TextView)findViewById(R.id.phonetextsec);
        phonevalue=(EditText) findViewById(R.id.phone_num);
        phonevalues=(EditText) findViewById(R.id.phone_numsec);
        emailtext=(TextView)findViewById(R.id.emailtxt);
        addresstxt=(TextView)findViewById(R.id.addresstext);
        emailvalue=(EditText) findViewById(R.id.email_address);
        addressval=(EditText) findViewById(R.id.addressvalue);
        passText=(TextView) findViewById(R.id.passtxt);
        address=(EditText) findViewById(R.id.pass_txt);
        editButton=(Button)findViewById(R.id.submitprofile_button);
        gstntext.setTypeface(face);
        gstnvalue.setTypeface(face);
        storetext.setTypeface(face);
        storevalue.setTypeface(face);
        nametext.setTypeface(face);
        namevalue.setTypeface(face);
        phonetext.setTypeface(face);
        phonevalue.setTypeface(face);
        addresstxt.setTypeface(face);
        addressval.setTypeface(face);
        emailtext.setTypeface(face);
        emailvalue.setTypeface(face);
        address.setTypeface(face);
        toolbartext.setTypeface(face);
        gstnvalue.setText(profileModel.getStoreGst());
        storevalue.setText(profileModel.getStoreName());
        namevalue.setText(profileModel.getPersonName());
        phonevalue.setText(profileModel.getPrimaryNumber());
        phonevalues.setText(profileModel.getSecoundaryNumber());
        emailvalue.setText(profileModel.getsEmail());
        addressval.setText(profileModel.getAddress());
        SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!ValidateEmail()){
                    return;
                }else{
                   postProfileEdit(customerIdValue);
                }
            }
        });

    }

    public void postProfileEdit(final String idValue){
        progressDialog.show();
        String url=URLInfoConstants.API_URL+"callback&service=updateuser";
        //&customer_id=12&firstname=Sha&email=sha@gmail.com&telephone=9948484627&second_number=9948484627&password=
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressDialog.dismiss();
                        Log.d("Response", response);
                        Toast.makeText(EditProfileActivity.this,"Profile Sucessfully Edited",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        progressDialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
              //  https://powertheatre.kloudportal.com/axiom?callback&service=updateuser&customer_id=18&firstname=Sha&email=sha143@gmail.com&telephone=9948484627&second_number=9948484627&gstin_no=36AHH23572&store_name=KTC&store_address=Hyderabad
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", idValue);
                params.put("firstname", namevalue.getText().toString().trim());
                params.put("email", emailvalue.getText().toString().trim());
                params.put("telephone",phonevalue.getText().toString().trim());
                params.put("second_number",phonevalues.getText().toString().trim());
                params.put("gstin_no",gstnvalue.getText().toString().trim() );
                params.put("store_address",addressval.getText().toString().trim());


                return params;
            }

        };

        queue.add(putRequest);
    }


    public String encodeToBase64(String input) {
        byte[] data = new byte[0];
        try {
            data = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String base64Encoded = Base64.encodeToString(data, Base64.NO_WRAP);

        return base64Encoded;

    }
    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(emailvalue.getText().toString().trim())) {
            Toast.makeText(EditProfileActivity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
