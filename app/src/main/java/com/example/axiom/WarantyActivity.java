package com.example.axiom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kloudportal.axiom.powertheater.R;

import java.util.HashMap;
import java.util.Map;

public class WarantyActivity extends AppCompatActivity {
    private Toolbar toolbar;
    Button warantysubmit,takephoto;
    TextView toolbartext, serialnum,mobiletext,warantyheader;
    EditText serialvalue,mobilNum;
    String customerIdValue;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waranty);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        dialog=new ProgressDialog(WarantyActivity.this,R.style.MyAlertDialogStyle);
                   dialog.setMessage("Please wait Loading.....");
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Warranty Form");
        toolbartext.setTypeface(face);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        warantyheader=(TextView)findViewById(R.id.headwaranty);
        serialnum=(TextView)findViewById(R.id.serialtext);
        mobiletext=(TextView)findViewById(R.id.pricetext);
        serialvalue=(EditText) findViewById(R.id.serial_num);
        mobilNum=(EditText) findViewById(R.id.pricevalue);
        warantysubmit=(Button) findViewById(R.id.submitwaranty_button);
        takephoto=(Button) findViewById(R.id.photo_button);

        SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);

         Intent intent =getIntent();
         if(intent.hasExtra("serialno")){
             serialvalue.setText(intent.getStringExtra("serialno"));
         }
         takephoto.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 startActivity(new Intent(WarantyActivity.this,ScanActivity.class));

             }
         });

         warantysubmit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
              if(serialvalue != null && !TextUtils.isEmpty(mobilNum.getText().toString().trim())){
                  postSubmitWarrenty();
              }else{
                  Toast.makeText(WarantyActivity.this,"Please Enter All Fields",Toast.LENGTH_LONG).show();
              }
             }
         });

        serialnum.setTypeface(face);
        mobiletext.setTypeface(face);
      //  serialvalue.setTypeface(face);
      //  mobilNum.setTypeface(face);
        warantyheader.setTypeface(face);


    }
    public  void postSubmitWarrenty(){

        dialog.show();

        String url=URLInfoConstants.API_URL+"callback&service=warranty_ticket&store=1";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        dialog.dismiss();
                        Log.d("Response", response);
                        Toast.makeText(WarantyActivity.this,"Warranty Submitted Successfully",Toast.LENGTH_LONG).show();
                        finish();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                //&customer_id=12&serial_no=12345&telephone=9948484672
                 params.put("customer_id",customerIdValue);
                 params.put("serial_no",serialvalue.getText().toString().trim());
                 params.put("telephone",mobilNum.getText().toString().trim());


                return params;
            }

        };

        queue.add(putRequest);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
