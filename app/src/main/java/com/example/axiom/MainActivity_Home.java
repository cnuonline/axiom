package com.example.axiom;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;


import com.example.axiom.Fragment.ContactFragment;
import com.example.axiom.Fragment.HistoryFragment;
import com.example.axiom.Fragment.HomeFragment;
import com.example.axiom.Fragment.ProfileFragment;
import com.example.axiom.Fragment.ServiceFragment;
import com.example.axiom.Fragment.Service_List_Fragment;
import com.example.axiom.Fragment.VideoFragment;
import com.example.axiom.Fragment.Warranty_List_Fragment;
import com.kloudportal.axiom.powertheater.R;

import java.util.ArrayList;


public class MainActivity_Home extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    Context context;
    RelativeLayout profileBox;
    RelativeLayout mDrawerPane;
    ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private BroadcastReceiver mNetworkReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setNavigationRecylerViw();


        changeFragment(new HomeFragment());

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @SuppressLint("NewApi") @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setNavigationRecylerViw();
                //invalidateOptionsMenu();
            }

            @SuppressLint("NewApi") @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d("closed", "onDrawerClosed: " + getTitle());
                invalidateOptionsMenu();
            }
        };



        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        context= MainActivity_Home.this;

    }

    private void setNavigationRecylerViw() {
        mNavItems.clear();
        mNavItems.add(new NavItem("Home",  R.drawable.ic_home_black_24dp));
        mNavItems.add(new NavItem("Orders/History", R.drawable.ic_playlist_add_check_black_24dp));
        mNavItems.add(new NavItem("Warranty",  R.drawable.ic_all_inclusive_black_24dp));
//        mNavItems.add(new NavItem("Order Now", R.drawable.ic_attach_money_black_24dp));
        mNavItems.add(new NavItem("Service/Repair", R.drawable.ic_settings_black_24dp));
        mNavItems.add(new NavItem("Promotional Videos", R.drawable.ic_videocam_black_24dp));
        mNavItems.add(new NavItem("Find Us", R.drawable.ic_phone_black_24dp));
        mNavItems.add(new NavItem("Profile",  R.drawable.ic_person_outline_black_24dp));
        mNavItems.add(new NavItem("Logout",  R.drawable.logout));


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        profileBox=(RelativeLayout)findViewById(R.id.profileBox);
        profileBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openorclose();
            }
        });

        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);



        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                selectItemFromDrawer(position);

            }
        });
    }



    public  void openorclose()
    {

        //final ImageView myImage = (ImageView) findViewById(R.id.rightclick);
        //myImage.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate) );
        if(mDrawerLayout == null)
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        //checking whether drawerlayout is opened or not
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
        {
            //mDrawerLayout.openDrawer(Gravity.RIGHT);
            mDrawerLayout.closeDrawers();

        }else{
            mDrawerLayout.openDrawer(Gravity.START);

        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void selectItemFromDrawer(int position) {

        android.support.v4.app.Fragment fragment=null;
        if(position == 0){

        fragment= new HomeFragment();

        }else if(position ==1){
            fragment= new HistoryFragment();
        }else if(position ==2){
            if(!runtime_permissions())
                nextactivity();

        }else if(position ==3){
            fragment= new ServiceFragment();
        }
        else if(position ==4){
            fragment= new VideoFragment();

        }else if(position ==5){

            fragment= new ContactFragment();
        }else if(position ==6){

            fragment= new ProfileFragment();
        }else if(position ==7){
            SharedPreferences sharedPreferences= getSharedPreferences("LoginPref", MODE_PRIVATE);
            SharedPreferences.Editor editor1= sharedPreferences.edit();
            editor1.clear();
            editor1.commit();
            editor1.apply();
            Intent intent = new Intent(MainActivity_Home.this,Login_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(intent);
            finish();
        }

        if(fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            transaction.replace(R.id.mainContent, fragment);
            transaction.commitAllowingStateLoss();
        }

        // mDrawerList.setItemChecked(position, true);
        //setTitle(mNavItems.get(position).mTitle);
        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
    private void changeFragment(Fragment targetFragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainContent, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


    private boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(MainActivity_Home.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            //Alert boz to show user what permissions has to be granted
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity_Home.this);
            alertDialogBuilder.setTitle("App Permissions");
//            alertDialogBuilder.setMessage(R.string.ALLinone);
            alertDialogBuilder.setCancelable(false);
            //If user allow ask permission
            alertDialogBuilder.setPositiveButton("Allow",
                    new DialogInterface.OnClickListener() {

                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            requestPermissions(new String[]{
                                    Manifest.permission.CAMERA
                            }, 1);
                        }
                    });

            alertDialogBuilder.setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //If user deny then close app
                     finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


            return true;
        }
        return false;
    }
    public  void nextactivity(){
        startActivity(new Intent(MainActivity_Home.this, WarantyActivity.class));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                nextactivity();


            }
            else
            {
                runtime_permissions();

            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        finish();
        startActivity(getIntent());
        super.onRestart();
    }
}
