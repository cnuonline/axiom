package com.example.axiom;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kloudportal.axiom.powertheater.R;

import java.util.List;


public class MyRecycler_history_list_Adapter extends RecyclerView
        .Adapter<MyRecycler_history_list_Adapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    List<OrderModel> myDataset;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            {
        TextView  orderId,status,quantityvalue;


        public DataObjectHolder(View itemView) {
            super(itemView);

             orderId=(TextView)itemView.findViewById(R.id.odernumber);
             status=(TextView)itemView.findViewById(R.id.statusvalue);
             quantityvalue=(TextView)itemView.findViewById(R.id.quantitynumber);

        }


    }

   public  MyRecycler_history_list_Adapter(){

   }

    public MyRecycler_history_list_Adapter(List<OrderModel> myDataset) {
       this.myDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_history, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        context=parent.getContext();
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");

        OrderModel orderModel=myDataset.get(position);
        holder.orderId.setText(orderModel.getOrderId());
        holder.orderId.setTypeface(face);
        holder.quantityvalue.setText(orderModel.getOrderQuantity());
        holder.quantityvalue.setTypeface(face);
        holder.status.setText(orderModel.getOrderStatus());
        holder.status.setTypeface(face);

    }

       public void addOrders(List<OrderModel> orderModels){

        for(OrderModel orderModel:orderModels){
            myDataset.add(orderModel);
        }
           notifyDataSetChanged();

       }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }





}