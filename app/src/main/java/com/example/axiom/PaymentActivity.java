package com.example.axiom;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.axiom.AddCart.ShoppingCart;
import com.kloudportal.axiom.powertheater.R;

import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity {
      Spinner paymentSpinner;
    private boolean isDisableExitConfirmation = false;
    private Toolbar toolbar;

    Button submit_Payment;
      List<String> paymentArayList =new ArrayList<>();
      String addressValue="";
      String productValue="";
      String customerIdValue,paymentName,emailCustomer,contactCustomer,firstNameCustomer;
      String orderId,grand_Total,taxAmount,subtotal;
      ProgressDialog progressDialog;
    TextView toolbartext;
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        toolbar = (Toolbar) findViewById(R.id.toolbr);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        paymentSpinner=(Spinner)findViewById(R.id.spinnerpayment);
        submit_Payment=(Button)findViewById(R.id.submit_button);
        progressDialog=new ProgressDialog(PaymentActivity.this,R.style.MyAlertDialogStyle);
                           progressDialog.setMessage("Loading...");
        getPaymentMethods();
        Intent intent=getIntent();
        if(intent.hasExtra("addressvalue")){
            addressValue=intent.getStringExtra("addressvalue");
            productValue=intent.getStringExtra("productsvalue");
            contactCustomer=intent.getStringExtra("contactnumber");
            firstNameCustomer=intent.getStringExtra("firstname");
        }
        SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
        customerIdValue=sharedPreferences.getString("user_id",null);
        emailCustomer=sharedPreferences.getString("email",null);
       paymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               paymentName=paymentArayList.get(i);
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });


       submit_Payment.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(paymentName.equalsIgnoreCase("Payment Methods")){
               }
           }
       });
        getOrderId(addressValue,productValue,customerIdValue);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void getPaymentMethods(){
         progressDialog.show();
        String url=URLInfoConstants.API_URL+"callback&store=1&service=getpaymentmethod";

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            Log.d("responsePayment",response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                     JSONObject paymentObject=jsonObject.getJSONObject("pumbolt");
                     paymentArayList.add("Payment Methods");
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaymentActivity.this,
                            R.layout.spinner_item_text,paymentArayList);

                    // attaching data adapter to spinner
                    paymentSpinner.setAdapter(adapter);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            Log.d("errorpayment",error.toString());
            }
        });
        Volley.newRequestQueue(PaymentActivity.this).add(stringRequest);
    }

       public void getOrderId(final String adress, final String products, final String customerId){
         progressDialog.show();
        String url=URLInfoConstants.API_URL+"callback&store=1&currency=INR&service=placeorder";
        //&customerid="+customerId+"&products="+products+"&is_create_quote=1&transactionid=12346&paymentmethod=pumbolt&address="+adress
           RequestQueue queue = Volley.newRequestQueue(this);
           StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                   new Response.Listener<String>()
                   {
                       @Override
                       public void onResponse(String response) {
                           // response
                           if(progressDialog.isShowing())
                               progressDialog.dismiss();

                           Log.d("Response", response);

                           try {
                               JSONObject jsonObject=new JSONObject(response);
                                 orderId=jsonObject.getString("orderid");
                                 grand_Total=jsonObject.getString("grand_total");
                               taxAmount=jsonObject.getString("tax_amount");
                               subtotal=jsonObject.getString("subtotal");
                                 launchPayUMoneyFlow();
                           } catch (JSONException e) {
                               e.printStackTrace();
                           }

                       }
                   },
                   new Response.ErrorListener()
                   {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                           // error
                           if(progressDialog.isShowing())
                               progressDialog.dismiss();
                           Log.d("Error.Response", error.toString());
                       }
                   }
           ) {

               @Override
               protected Map<String, String> getParams()
               {
                   Map<String, String> params = new HashMap<String, String>();

                    params.put("customerid",customerId);
                    params.put("products",products);
                    params.put("is_create_quote","1");
                    params.put("transactionid","12345");
                    params.put("paymentmethod","pumbolt");
                    params.put("address",adress);


                   return params;
               }

           };
           putRequest.setRetryPolicy(new RetryPolicy() {
               @Override
               public int getCurrentTimeout() {
                   return 50000;
               }

               @Override
               public int getCurrentRetryCount() {
                   return 50000;
               }

               @Override
               public void retry(VolleyError error) throws VolleyError {

               }
           });
           queue.add(putRequest);


    }
    private void launchPayUMoneyFlow() {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button
        payUmoneyConfig.setDoneButtonText("Sucessfully Done");

        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle("Axiom Payment");

        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        double amount = 0;
        try {
            amount = Double.parseDouble(grand_Total);

        } catch (Exception e) {
            e.printStackTrace();
        }
        String txnId = orderId;
        //String txnId = "TXNID720431525261327973";
        String phone =contactCustomer;
        String productName ="Axiom";
        String firstName =firstNameCustomer;
        String email = emailCustomer;
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        builder.setAmount(String.valueOf(amount))
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")
                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(false)
                .setKey("3SB78QVx")
                .setMerchantId("6673300");

        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            //    generateHashFromServer(mPaymentParams);

            /*            *//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);


                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,PaymentActivity.this, R.style.AppTheme_default, true);


        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }
    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        stringBuilder.append("oBb5B7btUb");

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }
    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

            // Check which object is non-null
            String merchantResponse = transactionResponse.getTransactionDetails();

            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    Log.d("response",merchantResponse);
                    String payuResponse = transactionResponse.getPayuResponse();
                    getSucessUrl();

                   /* new AlertDialog.Builder(this)
                            .setCancelable(false)
                            .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {


                                }
                            }).show();*/
               } else {
                    //Failure Transaction
                    Log.d("error","error");
                    getFailureUrl();
                }

                // Response from Payumoney


                // Response from SURl and FURL



            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d("TAG", "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d("TAG", "Both objects are null!");
            }
        }
    }

      public  void getSucessUrl(){

                 String url=URLInfoConstants.API_URL+"callback&service=success_order&order_id="+orderId;

                  StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                      @Override
                      public void onResponse(String response) {
                          Toast.makeText(PaymentActivity.this,"Payment Sucess",Toast.LENGTH_LONG).show();
                          ShoppingCart.getInstance().clearCart();
                          Intent intent=new Intent(PaymentActivity.this,OrderAcknowledgement.class);
                          intent.putExtra("orderid",orderId);
                          intent.putExtra("total",grand_Total);
                          intent.putExtra("tax",taxAmount);
                          intent.putExtra("subtotal",subtotal);
                          startActivity(intent);
                          finish();

                      }
                  }, new Response.ErrorListener() {
                      @Override
                      public void onErrorResponse(VolleyError error) {

                      }
                  });

                Volley.newRequestQueue(PaymentActivity.this).add(stringRequest);
    }
    public  void getFailureUrl(){

        String url=URLInfoConstants.API_URL+"callback&service=failure_order&order_id="+orderId;

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
             Toast.makeText(PaymentActivity.this,"Payment Failure",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(PaymentActivity.this).add(stringRequest);
    }
}
