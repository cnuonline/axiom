package com.example.axiom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;

import com.example.axiom.AddCart.ShoppingCart;
import com.example.axiom.AddCart.ShoppingCartItem;
import com.example.axiom.AddCart.ShoppingItem;
import com.example.axiom.Fragment.MyCartFragment;
import com.kloudportal.axiom.powertheater.R;


import java.util.ArrayList;

/**
 * Created by kiran on 6/11/15.
 */
public class ShoppingCartAdapter extends BaseAdapter {
    private final Typeface lat0_Font_ttf, open_sans_regular, open_sans_semibold;
    private final LayoutInflater inflater;
    private final ArrayList<ShoppingCartItem> cartItemList;

    private MyCartFragment cartFragment;
    private Activity activity;

    public ShoppingCartAdapter(Activity activity, ArrayList<ShoppingCartItem> cartItemList, MyCartFragment cartFragment) {
        this.cartFragment = cartFragment;
        this.activity=activity;
        this.cartItemList=cartItemList;
        inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lat0_Font_ttf = Typeface.createFromAsset(activity.getAssets(), "fonts/lato-regular.ttf");
        open_sans_regular= Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Regular.ttf");
        open_sans_semibold= Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans-Semibold.ttf");
    }

    @Override
    public int getCount() {
        return cartItemList.size();
    }

    @Override
    public Object getItem(int i) {
        return cartItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        ViewHolder hodler;
        View row=convertView;
        if(convertView==null) {
            row=inflater.inflate(R.layout.row_product_added_my_cart_adapter,null);
            hodler=new ViewHolder();
            hodler.txtV_product_count=(TextView) row.findViewById(R.id.stockleft1);
            hodler.txtV_product_price=(TextView) row.findViewById(R.id.countvalue);
            hodler.txtV_product_name_mycart=(TextView) row.findViewById(R.id.productname1);
            hodler.ll_delete_added_itm = (ImageView) row.findViewById(R.id.ll_delete_added_itm);
            hodler.imgV_minus = (ImageView)row.findViewById(R.id.imgV_minus);
            hodler.imgV_plus  = (ImageView)row.findViewById(R.id.imgV_plus);

            row.setTag(hodler);
        } else {
            hodler = (ViewHolder) row.getTag();
        }

        ShoppingCartItem item =cartItemList.get(i);
        hodler.txtV_product_name_mycart.setText(item.getShoppingItem().getName());
        hodler.txtV_product_name_mycart.setTypeface(open_sans_regular);
        double subTotal = ShoppingCart.getInstance().getSubTotal(item.getShoppingItem());
        hodler.txtV_product_price.setText("\u20b9"+(subTotal));
        hodler.txtV_product_price.setTypeface(open_sans_semibold);

        hodler.txtV_product_count.setText(String.valueOf(item.getCount()));
        hodler.txtV_product_count.setTypeface(open_sans_regular);

        hodler.imageView = (ImageView)row.findViewById(R.id.product2);
        if(hodler.imageView != null) {
            ShoppingItem shoppingItem = item.getShoppingItem();
            String imageURL = shoppingItem.getThumbnail();//change for thumbnail

            if (imageURL != null) {
/*
                imageLoader.get(imageURL, ImageLoader.getImageListener(imageView, R.drawable.default_mofluid, R.drawable.default_mofluid));
*/           Glide.with(activity).load(item.getShoppingItem().getImage()).into(hodler.imageView);
            }
        }

        hodler.ll_delete_added_itm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartFragment.deleteItemFromCartList(i);
            }
        });

        hodler.imgV_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartFragment.lessItemCountToCart(cartItemList.get(i),i);
            }
        });

        hodler.imgV_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cartItemList.get(i).getShoppingItem().getType().toString().equals("downloadable"))
                    cartFragment.addItemValueToCart(cartItemList.get(i), i);
                else
                {
                    Toast.makeText(activity,"Single quantity for downloadable Product",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return row;
    }

    class ViewHolder {
        TextView txtV_product_count;
        TextView txtV_product_price;
        TextView txtV_product_name_mycart;
        ImageView ll_delete_added_itm;
        ImageView imgV_minus, imgV_plus;
        ImageView imageView;
    }
}
