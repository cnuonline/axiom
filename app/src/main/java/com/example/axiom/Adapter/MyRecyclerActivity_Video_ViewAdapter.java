package com.example.axiom.Adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.axiom.AddCart.ShoppingItem;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.kloudportal.axiom.powertheater.R;

import java.util.ArrayList;


public class MyRecyclerActivity_Video_ViewAdapter extends RecyclerView
        .Adapter<MyRecyclerActivity_Video_ViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    ArrayList<String> myDataset;
    private static MyClickListener myClickListener;
    Context context;
     final int UNINITIALIZED = 1;
     final int INITIALIZING = 2;
     final int INITIALIZED = 3;
    public static class DataObjectHolder extends RecyclerView.ViewHolder
            {
        TextView  productName;
        View linear;
                public YouTubeThumbnailView ytThubnailView = null;
                public ImageView ivYtLogo = null;
                public TextView tvTitle = null;

                final int UNINITIALIZED = 1;
                final int INITIALIZING = 2;
                final int INITIALIZED = 3;

        public DataObjectHolder(View itemView) {
            super(itemView);
            productName=(TextView)itemView.findViewById(R.id.tvNewFeature);
            linear=(LinearLayout)itemView.findViewById(R.id.lineralayout);
            ytThubnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.yt_thumbnail);
            ivYtLogo = (ImageView) itemView.findViewById(R.id.iv_yt_logo);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
           initialize();
        }

                public void initialize(){

                    ivYtLogo.setBackgroundColor(Color.parseColor("#fff"));
                    ytThubnailView.setTag(R.id.initialize, INITIALIZING);
                    ytThubnailView.setTag(R.id.thumbnailloader, null);
                    ytThubnailView.setTag(R.id.videoid, "");

                    ytThubnailView.initialize("AIzaSyA5kyaLgS7MKxS19uHf2CUsIOmDkv92DGU", new YouTubeThumbnailView.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                            ytThubnailView.setTag(R.id.initialize, INITIALIZED);
                            ytThubnailView.setTag(R.id.thumbnailloader, youTubeThumbnailLoader);

                            youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                                @Override
                                public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String loadedVideoId) {
                                    String currentVideoId = (String) ytThubnailView.getTag(R.id.videoid);
                                    if(currentVideoId.equals(loadedVideoId)) {
                                        ivYtLogo.setBackgroundColor(Color.parseColor("#000"));
                                    }
                                    else{
                                        ivYtLogo.setBackgroundColor(Color.parseColor("#fff"));
                                    }
                                }

                                @Override
                                public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                                    ivYtLogo.setBackgroundColor(Color.parseColor("#fff"));
                                }
                            });

                            String videoId = (String) ytThubnailView.getTag(R.id.videoid);
                            if(videoId != null && !videoId.isEmpty()){
                                youTubeThumbnailLoader.setVideo(videoId);
                            }
                        }

                        @Override
                        public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                            ytThubnailView.setTag(R.id.initialize, UNINITIALIZED);
                            ivYtLogo.setBackgroundColor(Color.parseColor("#fff"));
                        }
                    });
                }

    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
       this.myClickListener = myClickListener;
    }

    public MyRecyclerActivity_Video_ViewAdapter(ArrayList<String> myDataset) {
       this.myDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_layout_adapter, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        context=parent.getContext();
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {

        // int transparentColor = Color.parseColor("#0");
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
          holder.productName.setTypeface(face);
         // holder.productName.setText(myDataset.get(position));

        holder.tvTitle.setText("kiran");
        holder.ivYtLogo.setVisibility(View.VISIBLE);
        holder.ytThubnailView.setTag(R.id.videoid, myDataset.get(position));
        holder.ivYtLogo.setBackgroundColor(Color.parseColor("#fff"));

        int state = (int) holder.ytThubnailView.getTag(R.id.initialize);

        if(state == UNINITIALIZED){
            holder.initialize();
        }
        else if(state == INITIALIZED){
            YouTubeThumbnailLoader loader = (YouTubeThumbnailLoader) holder.ytThubnailView.getTag(R.id.thumbnailloader);
            loader.setVideo(myDataset.get(position));
        }
          holder.linear.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  myClickListener.onItemClick(position,view);
              }
          });
    }



    public void deleteItem(int index) {
        myDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}