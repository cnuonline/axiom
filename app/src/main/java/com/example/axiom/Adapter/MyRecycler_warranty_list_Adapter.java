package com.example.axiom.Adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.example.axiom.Warranty_Model;
import com.kloudportal.axiom.powertheater.R;


import java.util.ArrayList;
import java.util.List;


public class MyRecycler_warranty_list_Adapter extends RecyclerView
        .Adapter<MyRecycler_warranty_list_Adapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    List<Warranty_Model> myDataset;
    private static MyClickListener myClickListener;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            {
        TextView  serailNumber;
        TextView  serialDate;


        public DataObjectHolder(View itemView) {
            super(itemView);
            serailNumber=(TextView)itemView.findViewById(R.id.serialnum);
            serialDate=(TextView)itemView.findViewById(R.id.saledate);


        }


    }


    public MyRecycler_warranty_list_Adapter(List<Warranty_Model> myDataset) {
       this.myDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.warranty_list_adapter, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        context=parent.getContext();
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
         Warranty_Model warranty_model=myDataset.get(position);
         holder.serailNumber.setText(warranty_model.getSerialNum());
         holder.serialDate.setText(warranty_model.getSaleDate());


    }

    public void addItem(Warranty_Model dataObj, int index) {
        myDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        myDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
        public void onItemClickBuyNow(int position, View v);
    }
}