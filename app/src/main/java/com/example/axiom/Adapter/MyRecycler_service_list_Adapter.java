package com.example.axiom.Adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.axiom.Service_Model;
import com.kloudportal.axiom.powertheater.R;



import java.util.List;


public class MyRecycler_service_list_Adapter extends RecyclerView
        .Adapter<MyRecycler_service_list_Adapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    List<Service_Model> myDataset;
    private static MyClickListener myClickListener;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            {
        TextView  serailNumber,serialStatus;
        TextView  reason;
        TextView  description;
        CardView cardViewlayout;


        public DataObjectHolder(View itemView) {
            super(itemView);
            serailNumber=(TextView)itemView.findViewById(R.id.serialnum);
            reason=(TextView)itemView.findViewById(R.id.reason);
            description=(TextView)itemView.findViewById(R.id.descriptioon);
            serialStatus=(TextView)itemView.findViewById(R.id.serialstatus);
            cardViewlayout=(CardView)itemView.findViewById(R.id.cardlayout);

        }


    }



    public MyRecycler_service_list_Adapter(List<Service_Model> myDataset) {
       this.myDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_list_adapter, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        context=parent.getContext();
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
         Service_Model service_model=myDataset.get(position);
         holder.serailNumber.setText(service_model.getSerialNum());
         holder.reason.setText(service_model.getReason());
         holder.description.setText(service_model.getDesc());
         if(service_model.getStatus().equalsIgnoreCase("1")){
           holder.serialStatus.setText("Open");
         }else if(service_model.getStatus().equalsIgnoreCase("2")){
             holder.serialStatus.setText("In Progress");

         }else{
             holder.serialStatus.setText("Closed");

         }

    }

    public void addItem(Service_Model dataObj, int index) {
        myDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        myDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
        public void onItemClickBuyNow(int position, View v);
    }
}