package com.example.axiom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.kloudportal.axiom.powertheater.R;

import java.util.ArrayList;


/**
 * Created by kiran on 27/10/15.
 */
public class BannerAdapterProductDetails extends PagerAdapter {

    private final Activity activity;
    private final ArrayList<String> bannerDataList;
    /* public ImageLoader imageLoader;*/


    public BannerAdapterProductDetails(Activity act, ArrayList<String> bannerDataList) {
        activity = act;
        this.bannerDataList=bannerDataList;



    }

    public int getCount()
    {
        return bannerDataList.size();
    }

    public Object instantiateItem(View collection, final int position) {

        LayoutInflater inflater = (LayoutInflater) collection.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        final View page = inflater.inflate(R.layout.custom_images, null);
        ImageView imageView=(ImageView)page.findViewById(R.id.image);
       // imageView.setImageResource(bannerDataList.get(position).getBannrImage());
        Glide.with(activity)
                .load(bannerDataList.get(position)).into(imageView);

        page.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //this will log the page number that was click
                Log.i("TAG", "This page was clicked: " + position);
                String name = "SEARCH ITEMS";

               /* if(position==0){

                }
                else if(position==1){

                }
                else  if(position==2){

                }
                else{
                    Toast.makeText(activity,"All Brands Names",Toast.LENGTH_LONG).show();
                }*/

            }
        });

        ((ViewPager) collection).addView(page, 0);
        return page;
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1)
    {
        return arg0 == arg1;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
