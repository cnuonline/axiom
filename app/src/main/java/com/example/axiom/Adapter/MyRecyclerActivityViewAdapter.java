package com.example.axiom.Adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.example.axiom.AddCart.ShoppingItem;
import com.kloudportal.axiom.powertheater.R;

import java.util.ArrayList;


public class MyRecyclerActivityViewAdapter extends RecyclerView
        .Adapter<MyRecyclerActivityViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    ArrayList<ShoppingItem> myDataset;
    private static MyClickListener myClickListener;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            {
        ImageView imageViewOroduct;
        TextView  productName;
        TextView  priceValue;
        View relativeLayout;
        Button byNow;
        LinearLayout imageClick;
        TextView click;


        public DataObjectHolder(View itemView) {
            super(itemView);
            imageViewOroduct=(ImageView)itemView.findViewById(R.id.product1);
            productName=(TextView)itemView.findViewById(R.id.productname);
            priceValue=(TextView)itemView.findViewById(R.id.rupee);
            click=(TextView)itemView.findViewById(R.id.moretext);
            byNow=(Button) itemView.findViewById(R.id.buynow_button);
            relativeLayout=(LinearLayout)itemView.findViewById(R.id.recyclerviewlayout);
            imageClick=(LinearLayout)itemView.findViewById(R.id.imageclick);

        }



    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
       this.myClickListener = myClickListener;
    }

    public MyRecyclerActivityViewAdapter(ArrayList<ShoppingItem> myDataset) {
       this.myDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        context=parent.getContext();
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
          holder.productName.setTypeface(face);
        //  holder.priceValue.setTypeface(face);
          ShoppingItem shoppingItem=myDataset.get(position);
          holder.priceValue.setText("\u20b9"+shoppingItem.getPrice());
          holder.productName.setText(shoppingItem.getName());
          if(shoppingItem.getId().equalsIgnoreCase("1")){
              holder.byNow.setText("BUY NOW");

          }else{
              holder.byNow.setText("COMING SOON");
              holder.priceValue.setVisibility(View.GONE);
          }

          String url=shoppingItem.getImage();
        Glide.with(context)
                .load(url).into(holder.imageViewOroduct);

     holder.byNow.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             myClickListener.onItemClickBuyNow(position,view);
         }
     });
     holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             myClickListener.onItemClick(position,view);
         }
     });
     holder.imageClick.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             myClickListener.onItemImageclick(position,v);
         }
     });
        SpannableString string = new SpannableString("More...");
        string.setSpan(new UnderlineSpan(), 0, string.length(), 0);
        holder.click.setText(string);
    }

    public void addItem(ShoppingItem dataObj, int index) {
        myDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        myDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
        public void onItemClickBuyNow(int position, View v);
        public void onItemImageclick(int position, View v);
    }
}