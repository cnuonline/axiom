package com.example.axiom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kloudportal.axiom.powertheater.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Forgot_Activity extends AppCompatActivity {
    EditText email_id;
    Button submitbutton;
    private ProgressDialog dialog;
//    TextView login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_);
        dialog = new ProgressDialog(Forgot_Activity.this,R.style.MyAlertDialogStyle);
        dialog.setMessage("Loading ...");
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/arial.ttf");
        email_id= (EditText)findViewById(R.id.email);
        email_id.setTypeface(face);
        submitbutton= (Button) findViewById(R.id.submit_button);
        submitbutton.setTypeface(face);

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(email_id.getText().toString().trim())){
                    if(!ValidateEmail()){
                        return;
                    }else{
                         postForgotPass();
                    }
                }else{
                    Toast.makeText(Forgot_Activity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
                }

            }
        });

    }
    public void postForgotPass(){
        dialog.show();

        String  url = URLInfoConstants.API_URL+"callback&service=forgotPassword";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            Toast.makeText(Forgot_Activity.this,jsonObject.getString("response"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email_id.getText().toString().trim());


                return params;
            }

        };

        queue.add(putRequest);
    }

    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(email_id.getText().toString().trim())) {
            Toast.makeText(Forgot_Activity.this,"Please Enter Valid Email",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
