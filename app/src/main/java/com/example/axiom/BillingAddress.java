package com.example.axiom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.axiom.billing.AddressData;
import com.example.axiom.billing.UserManager;
import com.example.axiom.billing.UserProfileItem;
import com.kloudportal.axiom.powertheater.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BillingAddress extends AppCompatActivity {

    EditText firstName,lastName,mobileNumber,billingAddress,cityName,countryName,stateName,zipCode;
    Button submitButton;
   String customerIdValue;
   ProgressDialog progressDialog;
    private Toolbar toolbar;
    TextView toolbartext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_address);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartext= (TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbartext.setText("Shipping Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
         // hideKeyboard(BillingAddress.this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        progressDialog=new ProgressDialog(BillingAddress.this,R.style.MyAlertDialogStyle);
                           progressDialog.setMessage("Loading...");
           firstName=(EditText)findViewById(R.id.firstname);
           lastName=(EditText)findViewById(R.id.lastname);
           mobileNumber=(EditText)findViewById(R.id.mobilenumber);
           billingAddress=(EditText)findViewById(R.id.address);
           cityName=(EditText)findViewById(R.id.city);
           countryName=(EditText)findViewById(R.id.country);
           stateName=(EditText)findViewById(R.id.state);
           zipCode=(EditText)findViewById(R.id.zipcode);
           submitButton=(Button)findViewById(R.id.submit_button);
           SharedPreferences sharedPreferences=getSharedPreferences("user_login_pref",MODE_PRIVATE);
           customerIdValue=sharedPreferences.getString("user_id",null);
         getBillingAddress(customerIdValue);
           submitButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {

                   if(!validateMobile()){
                       return;
                   }else{
                     if(!TextUtils.isEmpty(firstName.getText().toString().trim())&&!TextUtils.isEmpty(mobileNumber.getText().toString().trim())&&!TextUtils.isEmpty(billingAddress.getText().toString().trim())&&!TextUtils.isEmpty(countryName.getText().toString()
                     .trim())&&!TextUtils.isEmpty(cityName.getText().toString().trim())&&!TextUtils.isEmpty(stateName.getText().toString().trim())){
                         postCallRegister(customerIdValue);
                     }  else{
                         Toast.makeText(BillingAddress.this,"Please Enter All the fields",Toast.LENGTH_LONG).show();
                     }
                   }
               }
           });


           }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
           public  void postCallRegister(final String idValue){
                       progressDialog.show();
               String url=URLInfoConstants.API_URL+"callback&service=billing_address";
               //&customer_id="+idValue+"&street=KTC&city=Hyderabad&state=Telangana&country=india&postcode=500081&telephone=9948484627
               RequestQueue queue = Volley.newRequestQueue(this);
               StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                       new Response.Listener<String>()
                       {
                           @Override
                           public void onResponse(String response) {
                               // response
                               Log.d("Response", response);
                                   if(progressDialog.isShowing())
                                       progressDialog.dismiss();

                               JSONObject strJSNobj = null;

                               try {
                                   strJSNobj = new JSONObject(response);
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }
                               try {
                                   AddressData addressData = null;

                                   JSONObject jsonObject = strJSNobj.getJSONObject("billing_address");
                                   try {
                                       String firstNames = jsonObject.getString("firstname");
                                       String lastNames = jsonObject.getString("firstname");
                                       String contactNos = jsonObject.getString("telephone");
                                       String citys = jsonObject.getString("city");
                                       String states = jsonObject.getString("state");
                                       String streets = jsonObject.getString("region");
                                       String countryIds = jsonObject.getString("country_id");
                                       String pinCodes = jsonObject.getString("postcode");

                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                                   getBillingAddress(strJSNobj.getString("id"));
                                   Intent intent =new Intent(BillingAddress.this,ShowBillingAndShiipingAddress.class);
                                   intent.putExtra("idCustomer",strJSNobj.getString("id"));
                                   startActivity(intent);
                                   AddressData addressDatas = AddressData.create(jsonObject);
                                   UserProfileItem activeUser = UserManager.getInstance().getUser();
                                   if (activeUser != null) {
                                       activeUser.setBillingAddress(addressDatas);
                                   }
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }



                           }
                       },
                       new Response.ErrorListener()
                       {
                           @Override
                           public void onErrorResponse(VolleyError error) {
                               // error
                               if(progressDialog.isShowing())
                                   progressDialog.dismiss();
                               Log.d("Error.Response", error.toString());
                           }
                       }
               ) {

                   @Override
                   protected Map<String, String> getParams()
                   {
                       Map<String, String> params = new HashMap<String, String>();
                       params.put("customer_id", idValue);
                       params.put("street", billingAddress.getText().toString().trim());
                       params.put("city", cityName.getText().toString().trim());
                       params.put("state",stateName.getText().toString().trim());
                       params.put("country", countryName.getText().toString().trim());
                       params.put("postcode", zipCode.getText().toString().trim());
                       params.put("telephone", mobileNumber.getText().toString().trim());
                       params.put("firstname", firstName.getText().toString().trim());

                       return params;
                   }

               };

               queue.add(putRequest);



               }
         public void getBillingAddress(String idValue){
                     progressDialog.show();
                    String url =URLInfoConstants.API_URL+"callback&service=get_address&customer_id="+idValue;

                    StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Response", response);
                            if(progressDialog.isShowing())
                                progressDialog.dismiss();
                            JSONObject strJSNobj = null;

                            try {
                                strJSNobj = new JSONObject(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                AddressData addressData = null;

                                JSONObject jsonObject = strJSNobj.getJSONObject("billing_address");
                                try {
                                    String firstNames = jsonObject.getString("firstname");
                                    String lastNames = jsonObject.getString("firstname");
                                    String contactNos = jsonObject.getString("telephone");
                                    String citys = jsonObject.getString("city");
                                    String states = jsonObject.getString("street");
                                    String streets = jsonObject.getString("region");
                                    String countryIds = jsonObject.getString("country_id");
                                    String pinCodes = jsonObject.getString("postcode");
                                       firstName.setText(firstNames);
                                       lastName.setText(lastNames);
                                       mobileNumber.setText(contactNos);
                                       cityName.setText(citys);
                                       stateName.setText(states);
                                       if(streets .equalsIgnoreCase("null")){
                                           billingAddress.setHint("Address");

                                       }else{
                                           billingAddress.setText(streets);
                                       }
                                       if(pinCodes.equalsIgnoreCase("000000")){
                                           zipCode.setHint("Pincode");

                                       }else{
                                           zipCode.setText(pinCodes);

                                       }
                                       countryName.setText(countryIds);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                getBillingAddress(strJSNobj.getString("id"));
                                AddressData addressDatas = AddressData.create(jsonObject);
                                UserProfileItem activeUser = UserManager.getInstance().getUser();
                                if (activeUser != null) {
                                    activeUser.setBillingAddress(addressDatas);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error.Response", error.toString());
                            if(progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    });

                  Volley.newRequestQueue(BillingAddress.this).add(stringRequest);



         }
    public boolean validateMobile() {
        if (!ProjectUtils.IsMobleValidation(mobileNumber.getText().toString().trim())) {
            Toast.makeText(BillingAddress.this,"Please Enter Valid Mobile Num",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
