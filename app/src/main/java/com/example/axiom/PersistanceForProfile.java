package com.example.axiom;

import java.util.ArrayList;

public class PersistanceForProfile {
    private static final PersistanceForProfile ourInstance = new PersistanceForProfile();

    public static PersistanceForProfile getInstance() {
        return ourInstance;
    }

    private PersistanceForProfile() {
    }
    ProfileModel profileModel=new ProfileModel();

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }
}
